import Alert from 'views/Alert/Alert'
import Routes from 'components/routes'
import React from 'react'
import { Toaster } from 'react-hot-toast'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles } from 'theme/styles'
import { store } from './store/store'
import { theme } from './theme'

// theme is the default theme, which you can alter

const App = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <Toaster position="bottom-right" />
          <GlobalStyles />
          <Routes />
          <Alert />
        </ThemeProvider>
      </Provider>
    </BrowserRouter>
  )
}

export default App
