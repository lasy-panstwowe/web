import { palette } from './palette'

export const typography = {
  h1: {
    color: '#000000',
    fontWeight: 500,
    fontSize: '40px',
    letterSpacing: '-0.24px',
    lineHeight: '47px',
  },
  h2: {
    color: palette.black,
    fontWeight: 500,
    fontSize: '32px',
    letterSpacing: '-0.24px',
    lineHeight: '38px',
  },
  h3: {
    color: palette.black,
    fontWeight: 500,
    fontSize: '24px',
    letterSpacing: '-0.06px',
    lineHeight: '28px',
  },
  h4: {
    color: palette.black,
    fontWeight: 500,
    fontSize: '18px',
    letterSpacing: '-0.06px',
    lineHeight: '21px',
  },
  h5: {
    color: palette.black,
    fontWeight: 600,
    fontSize: '16px',
    letterSpacing: '-0.05px',
    lineHeight: '20px',
  },
  h6: {
    color: palette.black,
    fontWeight: 600,
    fontSize: '14px',
    letterSpacing: '-0.05px',
    lineHeight: '20px',
  },
  subtitle1: {
    color: palette.black,
    fontWeight: 400,
    fontSize: '14px',
    letterSpacing: '-0.05px',
    lineHeight: '21px',
  },
  subtitle2: {
    color: palette.black,
    fontWeight: 400,
    fontSize: '12px',
    letterSpacing: '-0.05px',
    lineHeight: '14px',
  },
  body1: {
    color: palette.black,
    fontWeight: 400,
    fontSize: '14px',
    letterSpacing: '-0.05px',
    lineHeight: '16px',
  },
  body2: {
    color: palette.black,
    fontWeight: 400,
    fontSize: '12px',
    letterSpacing: '-0.04px',
    lineHeight: '14px',
  },
}
