import { colors } from '@mui/material'

const white = '#FFFFFF'
const black = '#000000'

export const palette = {
  black,
  white,
  primary: {
    contrastText: white,
    dark: '#3C5D54',
    main: '#3C5D54',
    light: '#78CD7E',
  },
  success: {
    contrastText: white,
    dark: colors.green[900],
    main: colors.green[600],
    light: colors.green[400],
  },
  info: {
    contrastText: white,
    dark: colors.blue[900],
    main: colors.blue[600],
    light: colors.blue[400],
  },
  warning: {
    contrastText: white,
    dark: colors.orange[900],
    main: colors.orange[600],
    light: colors.orange[400],
  },
  error: {
    contrastText: white,
    dark: '#ff3131',
    main: '#ff4a4a',
    light: '#ff6464',
  },
}
