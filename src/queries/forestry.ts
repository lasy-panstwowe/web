import { ForestryDTO } from 'components/elements/organisms/ForestryAddForm/hooks'
import API from './api'

export async function getDistricts() {
  const response = await API.get('/district')
  return response.data
}
export async function getDistrict(id: string) {
  const response = await API.get(`/district/${id}`)
  return response.data
}
export async function getInspectorate(id: string) {
  const response = await API.get(`/forestry-inspectorate/${id}`)
  return response.data
}
export async function getInspectorates() {
  const response = await API.get('/forestry-inspectorate')
  return response.data
}
export async function getDevices() {
  const response = await API.get('/device')
  return response.data
}
export async function addForestry(dto: ForestryDTO) {
  const response = await API.post('/forestry', dto)
  return response.data
}
export async function getForestry(id: string) {
  const response = await API.get(`/forestry/${id}`)
  return response.data
}

export async function deleteForestry(id: string) {
  const response = await API.delete(`/forestry/${id}`)
  return response.data
}
