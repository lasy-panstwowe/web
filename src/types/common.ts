export type FlexPositioning =
  | 'center'
  | 'space-between'
  | 'space-evenly'
  | 'flex-start'
  | 'start'
  | 'flex-end'
  | 'end'

export type DisplayCss = 'block' | 'flex'

export type TextVariants = 'h3' | 'body1' | 'body2'
