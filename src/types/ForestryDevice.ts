import { Point } from './Forestry'

export interface IForestryDevice {
  animalData: any
  defaulsCoords: Point
  events: IEvent[]
  fireData: IFireData[]
  forestry: string
  lastTime: string
  name: string
  sampleFreq: number
  sendFreq: number
  type: string
  _id: string
}
export interface IEvent {
  battery: number
  humidity: number
  smoke: number
  temperature: number
  timestamp: string
}
export interface IFireData {
  battery: number
  humidity: number
  smoke: number
  temperature: number
  timestamp: string
  _id: string
}
