export interface IDistrict {
  _id: string
  name: string
  forestryInspectorate: string
}
