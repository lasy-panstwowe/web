export interface Person {
  id: string
  name: string
  surname: string
  email: string
  phone: string
  forestryId: string
  emploeeType: string
}
