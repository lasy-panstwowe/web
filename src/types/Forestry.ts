import { IDistrict } from './District'
import { IForestryInspectorate } from './ForestryInspectorate'

export interface Forestry {
  _id: string
  name: string
  coords: Point[]
  district: string
}

export interface Point {
  lat: number
  lng: number
}
export interface IFullForestry {
  forestry: Forestry
  currentDistrict?: IDistrict
  currentInspectorate?: IForestryInspectorate
}
