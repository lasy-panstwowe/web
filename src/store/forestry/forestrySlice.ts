import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { API } from 'queries'
import { Forestry } from 'types/Forestry'

export const fetchAsyncForestries = createAsyncThunk('forestry/fetchAsyncForestries', async () => {
  const response = await API.get(`/forestry`)
  return response.data
})

interface ForestryState {
  forestries: Forestry[]
  loading: 'idle' | 'pending' | 'succeeded' | 'failed'
}

const initialState = {
  forestries: [],
  loading: 'idle',
} as ForestryState

const forestrySlice = createSlice({
  name: 'forestry',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAsyncForestries.pending, () => {
      console.log('Pending')
    })
    builder.addCase(fetchAsyncForestries.fulfilled, (state, { payload }) => {
      console.log('Fetched Successfully!', payload)
      return { ...state, forestries: payload }
    })
    builder.addCase(fetchAsyncForestries.rejected, () => {
      console.log('Rejected!')
    })
  },
})

export const getAllForestry = (state: { forestry: ForestryState }) => state.forestry.forestries
export default forestrySlice.reducer
