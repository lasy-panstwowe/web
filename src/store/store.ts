import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import { forestryReducer } from './forestry'

export const store = configureStore({
  reducer: {
    forestry: forestryReducer,
  },
})

// export type AppDispatch = typeof store.dispatch
// export type RootState = ReturnType<typeof store.getState>
// export type AppThunk<ReturnType = void> = ThunkAction<
//   ReturnType,
//   RootState,
//   unknown,
//   Action<string>
// >
