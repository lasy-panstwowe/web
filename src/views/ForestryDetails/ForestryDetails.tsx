import { ForestryDetails } from 'components/templates/ForestryDetails'
import { deleteForestry, getDistrict, getForestry, getInspectorate } from 'queries/forestry'
import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { IDistrict } from 'types/District'
import { Forestry } from 'types/Forestry'
import { IForestryInspectorate } from 'types/ForestryInspectorate'

export const ForestryDetailsView = () => {
  const navigate = useNavigate()
  const { id } = useParams()

  const [forestry, setForestry] = useState<Forestry | null>(null)
  const [inspectorate, setDistrict] = useState<IForestryInspectorate | null>(null)
  const [district, setInspectorate] = useState<IDistrict | null>(null)

  useEffect(() => {
    if (id)
      getForestry(id).then((forestryData) => {
        setForestry(forestryData)
        getDistrict(forestryData.district).then((districtData) => {
          setDistrict(districtData)
          getInspectorate(districtData.forestryInspectorate).then((forestryInspectorateData) => {
            setInspectorate(forestryInspectorateData)
          })
        })
      })
  }, [id])
  const editForestryElementAction = () => {
    if (id) navigate({ pathname: `/forestry/edit/${id}` })
  }
  const deleteForestryAction = () => {
    if (id) deleteForestry(id).then(() => navigate('/forestry'))
  }

  if (!forestry || !district || !inspectorate) return <div />
  return (
    <ForestryDetails
      editForestry={editForestryElementAction}
      deleteForestry={deleteForestryAction}
      forestry={forestry}
      district={district}
      inspectorate={inspectorate}
    />
  )
}
