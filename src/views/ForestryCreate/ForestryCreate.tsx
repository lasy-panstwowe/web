import React, { useEffect, useState } from 'react'
import { ForestryCreate } from 'components/templates/ForestryCreate'
import { IDistrict } from 'types/District'
import { IForestryInspectorate } from 'types/ForestryInspectorate'
import { ForestryDTO } from 'components/elements/organisms/ForestryAddForm/hooks'
import {
  addForestry,
  getDistrict,
  getDistricts,
  getForestry,
  getInspectorate,
  getInspectorates,
} from 'queries/forestry'
import { useNavigate, useParams } from 'react-router-dom'
import toast from 'react-hot-toast'
import { Forestry } from 'types/Forestry'

export const ForestryCreateView = () => {
  const navigate = useNavigate()
  const { id } = useParams()

  const [forestry, setForestry] = useState<Forestry>()
  const [inspectorate, setInspectorate] = useState<IForestryInspectorate>()
  const [district, setDistrict] = useState<IDistrict>()

  useEffect(() => {
    if (id)
      getForestry(id).then((forestryData) => {
        setForestry(forestryData)
        getDistrict(forestryData.district).then((districtData) => {
          setDistrict(districtData)
          getInspectorate(districtData.forestryInspectorate).then((forestryInspectorateData) => {
            setInspectorate(forestryInspectorateData)
          })
        })
      })
  }, [id])

  const [districts, setDistricts] = useState<IDistrict[]>([])

  useEffect(() => {
    getDistricts().then((data) => setDistricts(data))
  }, [])

  const [inspectorates, setInspectorates] = useState<IForestryInspectorate[]>([])

  useEffect(() => {
    getInspectorates().then((data) => setInspectorates(data))
  }, [])

  const onSubmit = (dto: ForestryDTO) => {
    addForestry(dto)
      .then(() => {
        navigate('/')
        toast.success('Leśnictwo dodane poprawnie')
      })
      .catch((e) => {
        toast.error(e.response.data.message)
      })
  }

  return (
    <ForestryCreate
      defaultForestry={forestry}
      defaultDistrict={district}
      defaultInspectorate={inspectorate}
      districts={districts}
      inspectorates={inspectorates}
      onSubmit={onSubmit}
    />
  )
}
