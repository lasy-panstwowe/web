import { ForestryDevices as ForestryDevicesTemplate } from 'components/templates/ForestryDevices'
import { getDevices } from 'queries/forestry'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { fetchAsyncForestries, getAllForestry } from 'store/forestry'
import { IForestryDevice } from 'types/ForestryDevice'

export const ForestryDevices = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const [devices, setDevices] = useState<IForestryDevice[]>([])

  const showForestryDeviceDetailsAction = (id: string) => {
    navigate({ pathname: `/forestry-devices/${id}` })
  }

  useEffect(() => {
    getDevices().then((data) => setDevices(data))
  }, [])
  useEffect(() => {
    dispatch(fetchAsyncForestries())
  }, [dispatch])

  const forestries = useSelector(getAllForestry)

  return devices.length && forestries.length ? (
    <ForestryDevicesTemplate
      showForestryDetails={showForestryDeviceDetailsAction}
      devices={devices}
      forestries={forestries}
    />
  ) : null
}
