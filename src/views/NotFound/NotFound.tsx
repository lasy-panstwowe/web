import React from 'react'
import { Navigate } from 'react-router-dom'

import { COMMON_PATHS } from 'components/routes/paths/common'

interface INavigateToNotFound extends React.FunctionComponent {
  Component: React.FunctionComponent
}

/**
 * Component - React node to be render
 * @returns React.FunctionComponent
 */
const NavigateToNotFound: INavigateToNotFound = () => {
  return <Navigate to={COMMON_PATHS.NOT_FOUND} replace />
}

const NotFound: React.FunctionComponent<Record<string, never>> = () => {
  return <div>route not found</div>
}

NavigateToNotFound.Component = NotFound

export default NavigateToNotFound
