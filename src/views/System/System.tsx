import * as React from 'react'
import { Grid, Typography } from '@mui/material'
import Button from '@mui/material/Button'

export const System = () => {
  return (
    <div style={{ height: '100%', width: '100%' }}>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
      >
        <Grid item xs={3}>
          <div>
            <Typography variant="h1" component="h2">
              System
            </Typography>
          </div>
          <Button variant="contained" size="large" href="forestry">
            Leśnictwo
          </Button>
          <Button variant="contained" size="large" href="forestry-workers">
            Pracownicy
          </Button>
        </Grid>
      </Grid>
    </div>
  )
}
