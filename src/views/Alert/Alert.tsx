import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import React, { useEffect } from 'react'

const Alert = () => {
  const [open, setOpen] = React.useState(false)
  const [dataAlert, setData] = React.useState<any>(null)

  const handleClose = () => {
    setOpen(false)
  }

  useEffect(() => {
    const eventSource = new EventSource('http://localhost:8080/device/on-emergency')
    eventSource.onmessage = ({ data }) => {
      const response = JSON.parse(data)
      setOpen(true)
      setData(response)
    }
  }, [])

  return (
    dataAlert && (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Alarm</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Zarejestrowano alarm o zagrożeniu pożarowym o czasie: {dataAlert.payload.timestamp} dla
            czujnika: ID: {dataAlert.id.slice(0, 6).toUpperCase()}, temperatura:{' '}
            {dataAlert.payload.temperature}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus>
            Zamknij
          </Button>
        </DialogActions>
      </Dialog>
    )
  )
}

export default Alert
