import { Forestry as ForestryTemplate } from 'components/templates/Forestry'
import { deleteForestry, getDistricts, getInspectorates } from 'queries/forestry'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { fetchAsyncForestries, getAllForestry } from 'store/forestry'
import { IDistrict } from 'types/District'
import { IForestryInspectorate } from 'types/ForestryInspectorate'

export const Forestry = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [districts, setDistricts] = useState<IDistrict[]>([])
  const [inspectorates, setInspectorates] = useState<IForestryInspectorate[]>([])

  const deleteForestryAction = (id: string) => {
    deleteForestry(id).then(() => dispatch(fetchAsyncForestries()))
  }

  const editForestryElementAction = (id: string) => navigate({ pathname: `/forestry/edit/${id}` })
  const showForestryDetailsAction = (id: string) => navigate({ pathname: `/forestry/${id}` })

  useEffect(() => {
    getInspectorates().then((data) => setInspectorates(data))
    getDistricts().then((data) => setDistricts(data))
  }, [])

  useEffect(() => {
    dispatch(fetchAsyncForestries())
  }, [dispatch])

  const forestries = useSelector(getAllForestry)

  return forestries.length ? (
    <ForestryTemplate
      deleteForestry={deleteForestryAction}
      editForestryElement={editForestryElementAction}
      showForestryDetails={showForestryDetailsAction}
      districts={districts}
      inspectorates={inspectorates}
      forestries={forestries}
    />
  ) : null
}
