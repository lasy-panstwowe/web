import React from 'react'
import { Person } from 'types/Person'
import { ForestryEmploees } from 'components/templates'

export const ForestryEmploeesPage: React.FunctionComponent<Record<string, never>> = () => {
  const mockData: Person[] = [
    {
      id: '1',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '2',
      name: 'Piotr',
      surname: 'Nowak',
      email: 'p.nowak@lp.pl',
      phone: '501343677',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '3',
      name: 'Michał',
      surname: 'Kamiński',
      email: 'm.kaminski@lp.pl',
      phone: '794343234',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '4',
      name: 'Anna',
      surname: 'Malinoska',
      email: 'a.malinoska@lp.pl',
      phone: '794343234',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '5',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '6',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '7',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '8',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
    {
      id: '9',
      name: 'Ola',
      surname: 'Olacka',
      email: 'SampleMail@xxx.com',
      phone: '123456789',
      forestryId: '???',
      emploeeType: 'Forester',
    },
  ]

  return <ForestryEmploees emploees={mockData} />
}
