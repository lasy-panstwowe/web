import { Box } from 'components/elements'
import styled from 'styled-components'

export const RootContainer = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`
export const MiddleSection = styled(Box)`
  width: 100%;
`
export const Content = styled.div`
  padding: 27px 32px;
  width: 100%;
`
