import EmployeeIcon from 'assets/images/employee.jpg'
import ForestryIcon from 'assets/images/forestry.jpg'
import DeviceIcon from 'assets/icons/device.svg'
import AddIcon from 'assets/images/add.png'
import { EMPLOYEE_PATHS } from 'components/routes/paths/employee'
import { FORESTRY_PATHS } from 'components/routes/paths/forestry'
import { FORESTRY_DEVICES_PATHS } from 'components/routes/paths/forestry-devices'

interface NavigationLinks {
  label: string
  href: string
  alt: string
  icon: string
}

export const NAVIGATION_LINKS: NavigationLinks[] = [
  {
    label: 'Wyświetl listę leśnictw',
    href: FORESTRY_PATHS.FORESTRY_LIST,
    alt: 'forestry icon',
    icon: ForestryIcon,
  },
  {
    label: 'Dodaj leśnictwo',
    href: FORESTRY_PATHS.FORESTRY_CREATE,
    alt: 'forestry icon',
    icon: AddIcon,
  },
  {
    label: 'Wyświetl pracowników leśnictwa',
    href: EMPLOYEE_PATHS.WORKERS_LIST,
    alt: 'forestry icon',
    icon: EmployeeIcon,
  },
  {
    label: 'Wyświetl listę czujników',
    href: FORESTRY_DEVICES_PATHS.FORESTRY_DEVICES_LIST,
    alt: 'forestry devices icon',
    icon: DeviceIcon,
  },
]
