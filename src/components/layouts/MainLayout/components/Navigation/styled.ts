import styled from 'styled-components'

export const Img = styled.img`
  height: 30px;
  min-width: 40px;
  margin-right: 25px;
`

export const Root = styled.div`
  padding: 26px 20px;
  position: relative;
`
