import React from 'react'
import { IconButton } from 'components/elements'
import { CloseIconStyled, Title, Root } from './styled'

interface ITopBar {
  isStatic: boolean
  onClose: () => void
}
const TopBar: React.FunctionComponent<ITopBar> = ({ isStatic, onClose }) => {
  return (
    <Root>
      <Title variant="h3">Nawigacja</Title>
      {!isStatic && (
        <IconButton onClick={onClose}>
          <CloseIconStyled />
        </IconButton>
      )}
    </Root>
  )
}

export default TopBar
