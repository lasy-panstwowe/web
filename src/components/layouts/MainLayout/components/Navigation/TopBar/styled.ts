import styled from 'styled-components'
import { Text } from 'components/elements'
import { ReactComponent as CloseIcon } from 'assets/icons/close.svg'

export const Title = styled(Text)`
  text-transform: uppercase;
`

export const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 26px;
`

export const CloseIconStyled = styled(CloseIcon)`
  width: 20px;
  height: 20px;
`
