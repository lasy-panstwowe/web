import React from 'react'
import { Link, Text } from 'components/elements'

import { NAVIGATION_LINKS } from './helpers'
import { Img, Root } from './styled'
import { TopBar } from './TopBar'

interface INavigation {
  onClose: () => void
  isStatic: boolean
}
const Navigation: React.FunctionComponent<INavigation> = ({ isStatic, onClose }) => {
  return (
    <Root>
      <TopBar isStatic={isStatic} onClose={onClose} />
      {NAVIGATION_LINKS.map(({ href, label, icon }) => (
        <Link key={href} to={href} onClick={!isStatic ? onClose : () => {}}>
          <Img src={icon} alt="ads" />
          <Text variant="body2">{label}</Text>
        </Link>
      ))}
    </Root>
  )
}

export default Navigation
