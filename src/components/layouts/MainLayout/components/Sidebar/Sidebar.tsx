import React from 'react'

import { Drawer, Logo } from 'components/elements'
import { Navigation } from '../Navigation'

export interface ISidebar {
  open: boolean
  isStatic: boolean
  onClose: () => void
}
const Sidebar: React.FunctionComponent<ISidebar> = ({ open, isStatic, onClose }) => {
  return (
    <Drawer isStatic={isStatic} open={open} position="left">
      <Logo />
      <Navigation onClose={onClose} isStatic={isStatic} />
    </Drawer>
  )
}

export default Sidebar
