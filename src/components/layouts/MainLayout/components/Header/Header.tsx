import React from 'react'
import { AppBar, IconButton, Text } from 'components/elements'
import { BurgerIconStyled } from './styled'

export interface IHeader {
  openSideDrawer: () => void
  isDesktop: boolean
}
const Header: React.FunctionComponent<IHeader> = ({ openSideDrawer, isDesktop }) => {
  return (
    <AppBar height="127px" justify={isDesktop ? 'flex-end' : 'space-between'}>
      {!isDesktop && (
        <IconButton onClick={openSideDrawer}>
          <BurgerIconStyled />
        </IconButton>
      )}
      <Text>test@test.pl</Text>
    </AppBar>
  )
}

export default Header
