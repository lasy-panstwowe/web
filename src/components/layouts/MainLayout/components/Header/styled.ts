import styled from 'styled-components'
import { ReactComponent as BurgerIcon } from 'assets/icons/menu.svg'

export const BurgerIconStyled = styled(BurgerIcon)`
  height: 40px;
  width: 40px;
`
