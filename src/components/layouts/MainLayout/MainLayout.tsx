import React from 'react'
import { Outlet } from 'react-router-dom'

import { Content, MiddleSection, RootContainer } from './styled'
import { Header, Sidebar } from './components'
import useMainLayout from './useMainLayout'

const MainLayout: React.FunctionComponent = () => {
  const { isDesktop, open, toggle } = useMainLayout()
  return (
    <RootContainer>
      <Sidebar open={open} isStatic={isDesktop} onClose={toggle} />
      <MiddleSection>
        <Header openSideDrawer={toggle} isDesktop={isDesktop} />
        <Content>
          <Outlet />
        </Content>
      </MiddleSection>
    </RootContainer>
  )
}

export default MainLayout
