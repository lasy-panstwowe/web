import React from 'react'
import { device } from 'helpers/mediaQuery'
import { useMediaQuery, useToggle } from 'utilities'

const useMainLayout = () => {
  const isDesktop: boolean = useMediaQuery(device.tablet)
  const { open, toggle } = useToggle()

  React.useLayoutEffect(() => {
    if (open) {
      toggle()
    }
    // eslint-disable-next-line
  }, [isDesktop])

  return {
    isDesktop,
    open,
    toggle,
  }
}

export default useMainLayout
