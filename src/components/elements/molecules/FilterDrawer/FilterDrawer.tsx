/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from '@mui/material'
import BackIcon from 'assets/icons/backButton.svg'
import { Drawer } from 'components/elements/atoms'
import React from 'react'
import styled from 'styled-components'

const Img = styled.img`
  height: 30px;
  width: 30px;
  margin-right: 10px;
  align-self: center;
  transform: rotate(180deg);
`
const Text = styled.text`
  font-size: 25px;
  text-align: center;
  color: white;
  font-weight: 400;
`
const ButtonText = styled.text`
  font-size: 16px;
  text-align: center;
  color: white;
  font-weight: 400;
`
const BUTTON_LEFT = {
  display: 'flex',
  marginRight: 1,
  cursor: 'pointer',
  background: '#3C5D54',
  paddingTop: 1,
  paddingBottom: 1,
  paddingLeft: 4,
  paddingRight: 4,
}
const BUTTON_RIGHT = {
  display: 'flex',
  marginBottom: 1,
  cursor: 'pointer',
}

interface IFilterDrawer {
  open: boolean
  onResetClick: () => void
  onSubmitClick: () => void
  hideFilter: () => void
  children: React.ReactNode
}
export const FilterDrawer: React.FunctionComponent<IFilterDrawer> = ({
  open = true,
  onSubmitClick,
  onResetClick,
  hideFilter,
  children,
}: IFilterDrawer) => {
  return (
    <Drawer
      style={{ display: 'flex', flex: 1 }}
      open={open}
      isStatic={false}
      position="right"
      backgroundColor="#78CD7E"
    >
      <Box
        sx={{
          display: 'flex',
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <Box>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              padding: 3,
              alignItems: 'center',
              cursor: 'pointer',
            }}
            onClick={hideFilter}
          >
            <Img src={BackIcon} alt="back" />
            <Text>Filtrowanie</Text>
          </Box>
          <Box sx={{ padding: 3 }}>{children}</Box>
        </Box>

        <Box
          sx={{
            display: 'flex',
            borderTopWidth: 1,
            borderTopStyle: 'solid',
            paddingTop: 3,
            paddingBottom: 3,
            alignItems: 'center',
            borderTopColor: 'white',
            justifyContent: 'space-around',
            flexDirection: 'row',
          }}
        >
          <Box sx={BUTTON_LEFT} onClick={onSubmitClick}>
            <ButtonText>Zastosuj</ButtonText>
          </Box>
          <Box sx={BUTTON_RIGHT} onClick={onResetClick}>
            <ButtonText>Resetuj</ButtonText>
          </Box>
        </Box>
      </Box>
    </Drawer>
  )
}
