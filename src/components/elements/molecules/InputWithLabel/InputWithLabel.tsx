import * as React from 'react'
import { IInput, Input, Label } from 'components/elements/atoms'

export const InputWithLabel: React.FunctionComponent<IInput> = ({
  id,
  name,
  value,
  onChange,
  type,
  defaultValue,
}) => {
  return (
    <div>
      <Label htmlFor={id}>{name}</Label>
      <Input
        defaultValue={defaultValue}
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={onChange}
      />
    </div>
  )
}
