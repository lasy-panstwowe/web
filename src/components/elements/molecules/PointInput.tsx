import React from 'react'
import { InputWithLabel } from 'components/elements/molecules'
import styled from 'styled-components'

export type PointInputProps = {
  id: number
  latitude: number
  longitude: number
  onClickDelete(event: PointChangedEvent): void
  onLatitudeChange(e: PointChangedEvent): void
  onLongtitudeChange(e: PointChangedEvent): void
}

export interface PointChangedEvent {
  id: number
  value: number
}

const Label = styled.p`
  font-weight: 700;
  font-size: 12px;
  line-height: 25px;
`

const Inputs = styled.div`
  display: flex;
  flex-direction: row;
  gap: 10px;
`

const DeleteButton = styled.button`
  color: red;
  background: none;
  border: none;
  margin-top: 25px;
  margin-left: 10px;
  cursor: pointer;
`

export const PointInput: React.FC<PointInputProps> = ({
  id,
  latitude,
  longitude,
  onLatitudeChange,
  onLongtitudeChange,
  onClickDelete,
}) => {
  const onDelete = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()

    onClickDelete({ id, value: 0 })
  }

  return (
    <>
      <Label>{`Punkt ${id.toString()}`}</Label>
      <Inputs>
        <InputWithLabel
          id={`lat-input-${id}`}
          type="number"
          name="Szerokość"
          value={`${latitude}`}
          onChange={(e) => onLatitudeChange({ id, value: +e.target.value })}
        />
        <InputWithLabel
          id={`long-input-${id}`}
          type="number"
          name="Długość"
          value={`${longitude}`}
          onChange={(e) => onLongtitudeChange({ id, value: +e.target.value })}
        />
        <DeleteButton onClick={onDelete}>X</DeleteButton>
      </Inputs>
    </>
  )
}

export default PointInput
