/* eslint-disable @typescript-eslint/no-explicit-any */
import { Pagination } from '@mui/material'
import { makeStyles } from '@mui/styles'
import React, { ChangeEvent } from 'react'

const useStyles = makeStyles(() => ({
  root: {
    '& > *': {
      justifyContent: 'center',
      display: 'flex',
      marginTop: 20,
    },
  },
  ul: {
    '& .MuiPaginationItem-root': {
      '&.Mui-selected': {
        background: '#3c5d54',
        color: 'white',
      },
    },
  },
}))

export const PaginationElement: React.FunctionComponent<{
  defaultValue: number
  count: number
  onChange: (event: ChangeEvent<unknown>, value: number) => void
}> = ({ count = 1, defaultValue = 1, onChange }) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div>
        <Pagination
          onChange={onChange}
          classes={{ ul: classes.ul }}
          count={count}
          defaultValue={defaultValue}
          variant="outlined"
          shape="rounded"
        />
      </div>
    </div>
  )
}
