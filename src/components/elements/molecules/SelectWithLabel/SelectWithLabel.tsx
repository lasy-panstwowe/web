import * as React from 'react'
import { ISelect, Label, Select } from 'components/elements/atoms'

export const SelectWithLabel: React.FunctionComponent<ISelect> = ({
  id,
  name,
  options,
  value,
  onChange,
  keyName,
}) => {
  return (
    <div>
      <Label htmlFor={id}>{name}</Label>
      <Select
        isClearable
        options={options}
        onChange={onChange}
        id={id}
        key={keyName}
        name={name}
        value={value}
      />
    </div>
  )
}
