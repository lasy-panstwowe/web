/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box, Typography } from '@mui/material'
import EditIcon from 'assets/icons/edit.svg'
import ScreenIcon from 'assets/icons/screen.svg'
import TrashIcon from 'assets/icons/trash.svg'
import React from 'react'
import styled from 'styled-components'

export interface TableDropDownProps {
  isSelected: boolean
  onClick: () => void
  onScreenDetailsClick?: () => void
  onEditClick?: () => void
  onDeleteClick?: () => void
}

const Img = styled.img`
  height: 20px;
  min-width: 30px;
  margin-right: 10px;
  fill: red;
  align-self: center;
`
const Text = styled.text`
  color: #999999;
  font-size: 14px;
`
const BUTTON = {
  flexDirection: 'row',
  display: 'flex',
  marginBottom: 1,
  cursor: 'pointer',
}

export const TableDropdown: React.FunctionComponent<TableDropDownProps> = ({
  isSelected,
  onClick,
  onDeleteClick,
  onScreenDetailsClick,
  onEditClick,
}: TableDropDownProps) => {
  return (
    <Typography
      component="div"
      variant="body1"
      style={{
        height: 25,
        width: 25,
        position: 'relative',
      }}
    >
      {isSelected && (
        <Box
          sx={{
            bgcolor: 'white',
            border: '0.5px solid',
            borderColor: '#999999',
            p: 1,
            boxShadow: 3,
            position: 'absolute',
            top: 25,
            right: '0%',
            zIndex: 'tooltip',
          }}
        >
          {onScreenDetailsClick && (
            <Box sx={BUTTON} onClick={onScreenDetailsClick}>
              <Img src={ScreenIcon} alt="ads" />
              <Text>Wyświetl</Text>
            </Box>
          )}
          {onEditClick && (
            <Box sx={BUTTON} onClick={onEditClick}>
              <Img src={EditIcon} alt="ads" />
              <Text>Edytuj</Text>
            </Box>
          )}
          {onDeleteClick && (
            <Box sx={BUTTON} onClick={onDeleteClick}>
              <Img src={TrashIcon} alt="ads" />
              <Text>Usuń</Text>
            </Box>
          )}
        </Box>
      )}

      <Box
        sx={{
          bgcolor: () => (isSelected ? '#C4C4C4' : 'white'),
          color: () => (isSelected ? 'white' : 'black'),
          border: '1px solid',
          borderColor: '#C4C4C4',
          lineHeight: 1.3,
          fontSize: 14,
          height: 25,
          wight: 25,
          textAlign: 'center',
          zIndex: 'modal',
          cursor: 'pointer',
        }}
        onClick={onClick}
      >
        ...
      </Box>
    </Typography>
  )
}
