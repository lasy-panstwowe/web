import React from 'react'
import { ButtonInGroupProps, Modal } from '@adminjs/design-system'

export type DeleteConfirmationModalProps = {
  isVisible: boolean
  onClickDelete: ButtonInGroupProps['onClick']
  onClickCloseModal: ButtonInGroupProps['onClick']
}

export const DeleteConfirmationModal: React.FC<DeleteConfirmationModalProps> = (props) => {
  const { isVisible, onClickDelete, onClickCloseModal } = props

  const modalProps = {
    label: 'Uwaga',
    icon: 'Warning',
    title: 'Czy na pewno chcesz usunąć wybrany element?',
    subTitle: 'Element zostanie natychmiastowo usunięty. Akcji nie można cofnąć.',
    onOverlayClick: () => onClickCloseModal(),
    onClose: () => onClickCloseModal(),
    buttons: [
      {
        icon: 'TrashCan',
        label: 'Zatwierdź',
        variant: 'danger',
        source: 'delete',
        mr: 'lg',
        mt: 'lg',
        onClick: onClickDelete,
      },
      {
        label: 'Anuluj',
        mr: 'lg',
        mt: 'lg',
        onClick: onClickCloseModal,
      },
    ],
  }

  return <>{isVisible && <Modal {...modalProps} />}</>
}

export default DeleteConfirmationModal
