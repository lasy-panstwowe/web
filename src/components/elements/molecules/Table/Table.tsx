/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Checkbox,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material'
import React, { useState } from 'react'
import styled from 'styled-components'
import { TableDropdown } from '../TableDropdown/TableDropdown'

export interface TableConfig {
  field: string
  label: string
  identifier?: boolean
  omit?: boolean
}

const HeaderRow = styled(TableRow)(() => ({
  backgroundColor: '#f4f6f8',
}))

export const TableElement: React.FunctionComponent<{
  rows: any[]
  config: TableConfig[]
  isCheckbox?: boolean
  isShortID?: boolean
  deleteElement?: (id: string) => void
  showDetails?: (id: string) => void
  editElement?: (id: string) => void
}> = ({ rows, config, isShortID, isCheckbox = true, deleteElement, showDetails, editElement }) => {
  const [selectedId, setSelectedId] = useState('')

  const filtered = config.filter(({ omit }) => !omit)
  const headers = filtered.map(({ label }) => label)
  const fileds = filtered.map(({ field }) => field)
  const id = config.find(({ identifier }) => identifier)?.field || ''

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <HeaderRow>
            {isCheckbox && (
              <TableCell padding="checkbox">
                <Checkbox />
              </TableCell>
            )}

            {headers.map((header) => (
              <TableCell key={header}>{header}</TableCell>
            ))}
            <TableCell />
          </HeaderRow>
        </TableHead>
        <TableBody>
          {rows.map((row: any) => (
            <TableRow key={row[id]} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              {isCheckbox && (
                <TableCell padding="checkbox">
                  <Checkbox />
                </TableCell>
              )}
              {fileds.map((field) => (
                <TableCell key={field}>
                  {isShortID && field === 'id' ? row.id.slice(0, 6).toUpperCase() : row[field]}
                </TableCell>
              ))}
              <TableCell>
                <TableDropdown
                  isSelected={selectedId === row[id]}
                  onClick={() => setSelectedId(selectedId === row[id] ? '' : row[id])}
                  onDeleteClick={deleteElement ? () => deleteElement(row[id]) : undefined}
                  onEditClick={editElement ? () => editElement(row[id]) : undefined}
                  onScreenDetailsClick={showDetails ? () => showDetails(row[id]) : undefined}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
