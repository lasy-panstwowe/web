import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Box, cssClass } from '@adminjs/design-system'

export const BreadcrumbLink = styled(Link)`
  color: ${({ theme }): string => theme.colors.grey40};
  font-family: ${({ theme }): string => theme.font};
  line-height: ${({ theme }): string => theme.lineHeights.default};
  font-size: ${({ theme }): string => theme.fontSizes.default};
  text-decoration: none;
  &:hover {
    color: ${({ theme }): string => theme.colors.primary100};
    font-weight: bolder;
  }
  &:after {
    content: '/';
    padding: 0 ${({ theme }): string => theme.space.default};
  }
  &:last-child {
    &:after {
      content: '';
    }
  }
`

export type BreadcrumbProps = {
  resourceName: string
  resourceHref: string
  actionName: string
}

export const Breadcrumbs: React.FC<BreadcrumbProps> = (props) => {
  const { resourceName, resourceHref, actionName } = props

  return (
    <Box flexGrow={1} className={cssClass('Breadcrumbs')}>
      <BreadcrumbLink to="/">System</BreadcrumbLink>
      <BreadcrumbLink to={resourceHref || '/'} className="is-active">
        {resourceName}
      </BreadcrumbLink>
      {actionName !== 'list' && <BreadcrumbLink to="#">{actionName}</BreadcrumbLink>}
    </Box>
  )
}

export default Breadcrumbs
