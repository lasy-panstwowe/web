import React from 'react'
import { ButtonGroup, TableCell, ButtonInGroupProps } from '@adminjs/design-system'

export type DropdownMenuProps = {
  resource: string
  recordId: string
  onClickDelete: ButtonInGroupProps['onClick']
}

export const TableMenuDropdown: React.FC<DropdownMenuProps> = (props) => {
  const { resource, recordId, onClickDelete } = props

  const buttons = [
    {
      icon: 'OverflowMenuHorizontal',
      variant: 'light' as const,
      label: undefined,
      'data-testid': 'actions-dropdown',
      buttons: [
        {
          icon: 'Screen',
          label: 'Wyświetl',
          variant: 'primary',
          source: 'show',
          href: `/${resource}/${recordId}/show`,
        },
        {
          icon: 'Edit',
          label: 'Edytuj',
          variant: 'primary',
          source: 'edit',
          href: `/${resource}/${recordId}/edit`,
        },
        {
          icon: 'TrashCan',
          label: 'Usuń',
          variant: 'danger',
          source: 'delete',
          onClick: onClickDelete,
        },
      ],
    },
  ]

  return (
    <TableCell key="options">
      <ButtonGroup buttons={buttons} size="sm" />
    </TableCell>
  )
}
