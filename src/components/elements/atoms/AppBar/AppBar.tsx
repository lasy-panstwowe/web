import React from 'react'
import { FlexPositioning } from 'types/common'

import { Root } from './styled'

export interface IAppBar {
  children: React.ReactNode
  height?: string
  justify?: FlexPositioning
}

const AppBar: React.FunctionComponent<IAppBar> = ({
  height = '50px',
  justify = 'flex-start',
  children,
}) => {
  return (
    <Root height={height} justify={justify}>
      {children}
    </Root>
  )
}

export default AppBar
