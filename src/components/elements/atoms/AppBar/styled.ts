import styled from 'styled-components'
import { FlexPositioning } from 'types/common'

interface IRoot {
  height?: string
  justify?: FlexPositioning
}

export const Root = styled.div<IRoot>`
  display: flex;
  align-items: center;
  justify-content: ${(props) => props.justify};
  position: sticky;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 800;
  background-color: #fff;
  border-bottom: 1px solid #999999;
  padding: 0 24px;
  height ${(props) => props.height};
`
