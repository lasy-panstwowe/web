import React from 'react'

import { ReactComponent as GreenMenuLogo } from 'assets/icons/GreenMenuLogo.svg'

const Logo = () => {
  return <GreenMenuLogo />
}

export default Logo
