import { Position } from './Drawer'

export interface IPlacement {
  top: number | string
  left: number | string
  right: number | string
  bottom: number | string
}

export const transforms: Record<Position, string> = {
  top: 'translateY(-100%)',
  right: 'translateX(100%)',
  bottom: 'translateY(100%)',
  left: 'translateX(-100%)',
}
export const placements: Record<string, IPlacement> = {
  top: {
    top: 0,
    right: 0,
    left: 0,
    bottom: 'unset',
  },
  right: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 'unset',
  },
  bottom: {
    right: 0,
    bottom: 0,
    left: 0,
    top: 'unset',
  },
  left: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 'unset',
  },
}
