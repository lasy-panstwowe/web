import styled from 'styled-components'
import { placements, transforms } from './helpers'
import { IDrawer, Position } from './Drawer'

export interface IDrawerWrapper {
  position: Position
  size?: IDrawer['size']
  open: IDrawer['open']
  isStatic: IDrawer['isStatic']
}
export const DrawerWrapper = styled.div<IDrawerWrapper>`
  display: block;
  min-width: ${(props) =>
    props.position !== 'top' && props.position !== 'bottom' && props.size
      ? props.size
      : props.isStatic && '300px'};
  height: ${(props) =>
    (props.position === 'top' || props.position === 'bottom') && props.size ? props.size : '100%'};
  z-index: 1000;
  transform: ${(props) => (!props.open && !props.isStatic ? transforms[props.position] : null)};
`

export interface IDrawerContent {
  position: Position
  size?: IDrawer['size']
  isStatic: IDrawer['isStatic']
  open: IDrawer['open']
  backgroundColor: IDrawer['backgroundColor']
}
export const DrawerContent = styled.div<IDrawerContent>`
  display: block;
  box-sizing: border-box;
  position: fixed;
  z-index: 1000;
  width: ${(props) =>
    props.position !== 'top' && props.position !== 'bottom' && props.size ? props.size : '300px'};
  ${(props) => {
    const p = placements[props.position]
    return `
    top: ${p.top};
    right: ${p.right};
    bottom: ${p.bottom};
    left: ${p.left};
    `
  }};
  transform: ${({ open, isStatic, position }) =>
    !open && !isStatic ? transforms[position] : null};
  transition: transform 0.4s ease-in-out;
  overflow-x: hidden;
  color: #000;
  background-color: ${(props) => props.backgroundColor};
  box-shadow: -10px 0px 10px rgba(0, 0, 0, 0.19);
`
