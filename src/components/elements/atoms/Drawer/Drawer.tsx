import React from 'react'
import { Overlay } from '../Overlay/Overlay'
import { DrawerWrapper, DrawerContent } from './styled'

export type Position = 'top' | 'right' | 'bottom' | 'left'

export interface IDrawer {
  open: boolean
  isStatic: boolean
  size?: string
  position?: Position
  onDismiss?: () => void
  backgroundColor?: string
  children: React.ReactNode
  style?: any
}
const Drawer: React.FunctionComponent<IDrawer> = ({
  open = false,
  isStatic = false,
  onDismiss = () => {},
  size = '',
  position = 'left',
  backgroundColor = '#fff',
  style,
  children,
}) => {
  return (
    <DrawerWrapper isStatic={isStatic} open={open} size={size} position={position}>
      <Overlay open={open} onClick={onDismiss} />
      <DrawerContent
        open={open}
        isStatic={isStatic}
        size={size}
        style={style}
        position={position}
        backgroundColor={backgroundColor}
      >
        {children}
      </DrawerContent>
    </DrawerWrapper>
  )
}

export default Drawer
