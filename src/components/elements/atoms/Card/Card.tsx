import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  background-color: #ffffff;
  padding: 40px 50px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`

export const Card: React.FunctionComponent<{ className?: string; children: React.ReactNode }> = ({
  className,
  children,
}) => <Wrapper className={className}>{children}</Wrapper>
