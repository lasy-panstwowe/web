// eslint-disable-next-line import/no-extraneous-dependencies
import { LatLngTuple } from 'leaflet'
import React, { useEffect, useState } from 'react'
import { MapContainer, Polyline, Circle, TileLayer, useMap } from 'react-leaflet'
import styled from 'styled-components'

const MyMap = styled(MapContainer)`
  width: 100%;
  height: 400px;
`

export interface IMap {
  points: Array<LatLngTuple>
}

const pathOptions = { color: 'black', weight: 1 }

const getBoundries = (points: LatLngTuple[]): LatLngTuple[] => {
  let latHigh = points[0][0]
  let latLow = points[0][0]
  let lonHigh = points[0][1]
  let lonLow = points[0][1]

  points.forEach(([lat, lon]) => {
    if (lat > latHigh) latHigh = lat
    if (lat < latLow) latLow = lat
    if (lon > lonHigh) lonHigh = lon
    if (lon < lonLow) lonLow = lon
  })

  return [
    [latHigh, lonHigh],
    [latLow, lonLow],
  ]
}

const getCenter = (boundries: LatLngTuple[]): LatLngTuple => {
  return [(boundries[0][0] + boundries[1][0]) / 2, (boundries[0][1] + boundries[1][1]) / 2]
}

const SetViewOnClick = ({ coords }: { coords: LatLngTuple }) => {
  const map = useMap()
  map.setView(coords, map.getZoom())

  return null
}

export const Map: React.FunctionComponent<IMap> = ({ points = [] }) => {
  const [p, setP] = useState<LatLngTuple[]>([])
  const [center, setCenter] = useState<LatLngTuple>([52.32, 20.6])

  useEffect(() => {
    if (points.length >= 1) {
      const mappedPoints = [...points, points[0]]
      setP(mappedPoints)
      const boundries = getBoundries(points)
      setCenter(getCenter(boundries))
    } else {
      setP([])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [points])

  return (
    <MyMap center={center} zoom={11} scrollWheelZoom={false}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Polyline positions={p} pathOptions={pathOptions} />
      {p.map((point) => (
        <Circle
          key={`${point[0]}.${point[1]}`}
          center={point}
          pathOptions={pathOptions}
          radius={20}
        />
      ))}
      <SetViewOnClick coords={center} />
    </MyMap>
  )
}
