import styled from 'styled-components'
import { DisplayCss, FlexPositioning } from 'types/common'

export interface IBox {
  display?: DisplayCss
  alignItems?: FlexPositioning
  justify?: FlexPositioning
}

export const Box = styled.div<IBox>`
  display: ${({ display }) => display};
  align-items: ${({ alignItems }) => alignItems};
  justify-content: ${({ justify }) => justify};
`
