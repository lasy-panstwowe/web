import React from 'react'
import { useLocation } from 'react-router-dom'
import { LinkStyled } from './styled'

interface ILink {
  children: React.ReactNode
  to: string
  onClick?: () => void
}
const Link: React.FC<ILink> = ({ children, to, ...rest }) => {
  const { pathname } = useLocation()

  return (
    <LinkStyled to={to} match={to === pathname} {...rest}>
      {children}
    </LinkStyled>
  )
}

export default Link
