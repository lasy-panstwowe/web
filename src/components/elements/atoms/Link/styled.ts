import styled from 'styled-components'
import { Link } from 'react-router-dom'

interface ILinkStyled {
  match: boolean
}
export const LinkStyled = styled(Link)<ILinkStyled>`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  & p {
    color: ${({ match }) => (match ? '#3C5D54' : '#000')};
    font-weight: ${({ match }) => (match ? 600 : 400)};
    &:hover {
      text-decoration: underline;
    }
  }
`
