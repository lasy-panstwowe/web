import React from 'react'
import { TextVariants } from 'types/common'

import { Body1, Body2, H3 } from './styled'

interface IText {
  children: React.ReactNode
  className?: string
  variant?: TextVariants
}
const Text: React.FunctionComponent<IText> = ({ children, variant = 'body1', className }) => {
  let Component = Body1

  switch (variant) {
    case 'h3':
      Component = H3
      break
    case 'body2':
      Component = Body2
      break
    default:
      break
  }

  return <Component className={className}>{children}</Component>
}

export default Text
