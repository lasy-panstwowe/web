import styled from 'styled-components'

export const Body1 = styled.p`
  font-size: 18px;
  font-weight: 400;
  color: #999999;
  letter-spacing: 0px;
`

export const Body2 = styled.p`
  font-size: 14px;
  color: #000;
  letter-spacing: 0px;
`

export const H3 = styled.h3`
  font-size: 14px;
  font-weight: 500;
  color: #000;
  letter-spacing: 0px;
`
