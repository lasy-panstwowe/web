import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.input`
  width: 100%;
  padding: 11px;
  border: 1px solid #c0c0ca;
  font-weight: 600;
  font-size: 12px;
`

export interface IInput {
  id: string
  name: string
  value?: string
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
  type?: 'text' | 'number'
  defaultValue?: string
}

export const Input: React.FunctionComponent<IInput> = ({
  id,
  name,
  value,
  onChange,
  type = 'text',
  defaultValue,
}) => {
  return (
    <Wrapper
      defaultValue={defaultValue}
      type={type}
      id={id}
      name={name}
      value={value}
      autoComplete="off"
      onChange={onChange}
    />
  )
}
