import styled from 'styled-components'

export const Count = styled.div`
  padding: 5px 0;
  width: 30px;
  border-radius: 15px;
  background-color: #3c5d54;
  color: #ffffff;
  text-align: center;
  display: inline-block;
  font-size: 14px;
  line-height: 20px;
  transform: translate(0, -5px);
`
