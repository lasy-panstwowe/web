import styled from 'styled-components'

export interface IOverlay {
  open?: boolean
}
export const Overlay = styled.div<IOverlay>`
  background-color: rgba(0, 0, 0, 0.3);
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  z-index: 900;
  display: ${({ open = true }): 'none' | null => (open ? null : 'none')};
`
