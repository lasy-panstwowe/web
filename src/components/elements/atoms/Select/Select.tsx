import React from 'react'
import ReactSelect, { ActionMeta, GroupBase, StylesConfig } from 'react-select'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const customStyles: StylesConfig<any, false, GroupBase<any>> = {
  singleValue: (provided) => ({
    ...provided,
    fontWeight: '700',
    fontSize: '12px',
  }),
  control: (provided, state) => ({
    ...provided,
    ...(state.isFocused
      ? { boxShadow: '0 0 0 1px #000', borderColor: '#000', '&:hover': { borderColor: '#000' } }
      : {}),
  }),
}

export interface IOption {
  value: string
  label: string
}

export interface ISelect {
  id: string
  name: string
  options: Array<IOption>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onChange?: (newValue: any, actionMeta: ActionMeta<any>) => void
  isClearable?: boolean
  value?: IOption | null
  keyName?: string
}

export const Select: React.FunctionComponent<ISelect> = ({
  id,
  name,
  options,
  onChange,
  isClearable,
  value,
  keyName,
}) => {
  return (
    <ReactSelect
      id={id}
      key={keyName}
      name={name}
      value={value}
      styles={customStyles}
      options={options}
      onChange={onChange}
      isClearable={isClearable}
    />
  )
}
