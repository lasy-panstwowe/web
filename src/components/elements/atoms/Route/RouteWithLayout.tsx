import React from 'react'
import { Route } from 'react-router-dom'

const RouteWithLayout: React.FunctionComponent<{
  path: string
  key: string
  layout: React.FunctionComponent<{ children: React.ReactNode }>
  component: React.FunctionComponent
}> = ({ path, layout: Layout, component: Component, ...rest }) => {
  return <Route path={path} element={<Layout>{Component && <Component />}</Layout>} {...rest} />
}

export default RouteWithLayout
