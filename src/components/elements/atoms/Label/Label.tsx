import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.label`
  font-weight: 700;
  font-size: 12px;
  line-height: 25px;
`

export interface ILabel {
  htmlFor: string
  children?: React.ReactNode
}

export const Label: React.FunctionComponent<ILabel> = ({ htmlFor, children }) => {
  return <Wrapper htmlFor={htmlFor}>{children}</Wrapper>
}
