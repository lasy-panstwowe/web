import React from 'react'

import { Button } from './styled'

interface IIconButton {
  children: React.ReactNode
  onClick: () => void
  className?: string
}
const IconButton: React.FunctionComponent<IIconButton> = ({ className, onClick, children }) => {
  return (
    <Button className={className} onClick={onClick}>
      {children}
    </Button>
  )
}

export default IconButton
