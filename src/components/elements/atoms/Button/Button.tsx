import React, { MouseEventHandler } from 'react'
import styled from 'styled-components'

const PrimaryButton = styled.button`
  appearance: none;
  padding: 10px;
  border: none;
  color: #ffffff;
  cursor: pointer;
  background: #3c5d54;
  min-width: 170px;
  font-weight: 700;
  border: 2px solid #3c5d54;
  height: 100%;
`

const DisabledButton = styled(PrimaryButton)`
  background-color: #ffffff;
  color: #3c5d54;
  cursor: default;
`

export interface IButton {
  onClick: MouseEventHandler<HTMLButtonElement>
  type?: 'button' | 'submit'
  children?: React.ReactNode
  disabled?: boolean
}

export const Button: React.FunctionComponent<IButton> = ({
  type = 'button',
  onClick = () => {},
  children,
  disabled,
  ...rest
}) => {
  const ButtonComponent = disabled ? DisabledButton : PrimaryButton
  return (
    <ButtonComponent type={type} onClick={onClick} {...rest}>
      {children}
    </ButtonComponent>
  )
}
