import React from 'react'
import userEvent from '@testing-library/user-event'
import { render, screen } from 'tests/test-utils'
import { Button } from './Button'

describe('src/components/elements/atoms/Button/Button.tsx', () => {
  const childrenLabel = 'childrenLabel'
  const propsLabel = 'propsLabel'
  let counter: number
  const handleOnClick = () => {
    counter += 1
  }

  beforeEach(() => {
    counter = 0
  })

  test('should add and fire onClick event to button component', () => {
    render(<Button onClick={handleOnClick} />)
    const buttonElement = screen.getByRole('button')

    expect(counter).toBe(0)
    userEvent.click(buttonElement)
    expect(counter).toBe(1)
  })

  test('should render node inside button', () => {
    render(
      <Button onClick={handleOnClick}>
        <p>{childrenLabel}</p>
      </Button>
    )

    const buttonElement = screen.getByRole('button')
    const pTestElement = screen.getByText(childrenLabel)

    expect(buttonElement.childElementCount).toBe(1)
    expect(pTestElement).toBeInTheDocument()
  })
})
