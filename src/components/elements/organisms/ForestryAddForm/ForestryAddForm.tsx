import React, { useEffect, useState } from 'react'
import { InputWithLabel, SelectWithLabel } from 'components/elements/molecules'
import styled from 'styled-components'
import { Button, IOption } from 'components/elements/atoms'
import { IDistrict } from 'types/District'
import { IForestryInspectorate } from 'types/ForestryInspectorate'
import { Forestry } from 'types/Forestry'
import { PointForm } from '../PointForm'
import { ForestryDTO, useForestryCreator } from './hooks'
import { IPoint } from '../PointForm/interfaces'

const Form = styled.form`
  display: flex;
  flex-flow: column;
  gap: 22px;
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
`

export interface IForestryAddForm {
  districts: IDistrict[]
  inspectorates: IForestryInspectorate[]
  defaultForestry?: Forestry
  defaultDistrict?: IDistrict
  defaultInspectorate?: IForestryInspectorate
  onSubmit(dto: ForestryDTO): void
}

export const ForestryAddForm: React.FunctionComponent<IForestryAddForm> = ({
  defaultForestry,
  defaultDistrict,
  defaultInspectorate,
  inspectorates,
  districts,
  onSubmit,
}) => {
  const { forestry, forestryDTO, setName, setDistrict, setCoords } = useForestryCreator()

  const [currentInspectorate, setInspectorate] = useState<IOption | null>(null)
  const [inspectorateOptions, setInspectorateOptions] = useState<IOption[]>([])
  const [districtOptions, setDistrictOptions] = useState<IOption[]>([])
  const [saveDisabled, setSaveDisabled] = useState(true)

  useEffect(() => {
    setInspectorateOptions(
      inspectorates.map((inspectorate) => ({
        value: inspectorate._id,
        label: inspectorate.name,
      }))
    )
  }, [inspectorates])

  useEffect(() => {
    setDistrictOptions(
      districts
        .filter((district) => district.forestryInspectorate === currentInspectorate?.value)
        .map((district) => ({
          value: district._id,
          label: district.name,
        }))
    )
  }, [districts, currentInspectorate])

  useEffect(() => {
    if (!districtOptions.find((option) => option.value === forestry.district?.value)) {
      setDistrict(null)
    }
  }, [districtOptions, forestry, setDistrict])

  useEffect(() => {
    const { name, districtId, coords } = forestryDTO
    if (name && districtId && coords.length >= 3) {
      setSaveDisabled(false)
    } else {
      setSaveDisabled(true)
    }
  }, [forestryDTO, setSaveDisabled])

  useEffect(() => {
    if (defaultForestry) {
      setName(defaultForestry?.name)
    }
    if (defaultDistrict) {
      setDistrict({
        value: defaultDistrict?._id,
        label: defaultDistrict?.name,
      })
    }
    if (defaultInspectorate) {
      setInspectorate({ value: defaultInspectorate._id, label: defaultInspectorate.name })
    }
  }, [defaultDistrict, defaultForestry, defaultInspectorate, setCoords, setName, setDistrict])

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  const onInspectorateChange = (newValue: IOption) => {
    setInspectorate(newValue || null)
  }

  const onDistrictChange = (newValue: IOption) => {
    setDistrict(newValue || null)
  }

  const onPointsChange = (points: IPoint[]) => {
    setCoords(points)
  }

  const onClick = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()

    if (!saveDisabled) {
      onSubmit(forestryDTO)
    }
  }
  return (
    <Form>
      <InputWithLabel
        id="name-input"
        name="Nazwa"
        value={forestry.name}
        onChange={handleNameChange}
      />
      <SelectWithLabel
        id="inspectorate-input"
        name="Nadleśnictwo"
        options={inspectorateOptions}
        onChange={onInspectorateChange}
      />
      <SelectWithLabel
        id="district-input"
        name="Obręb"
        value={forestry.district}
        options={districtOptions}
        onChange={onDistrictChange}
      />
      <PointForm
        defaultPoints={defaultForestry?.coords.map((coord, index) => ({
          longtitude: coord.lng,
          latitude: coord.lat,
          id: index,
        }))}
        onPointsChange={onPointsChange}
      />
      <ButtonWrapper>
        <Button type="submit" onClick={onClick} disabled={saveDisabled}>
          Zapisz
        </Button>
      </ButtonWrapper>
    </Form>
  )
}
