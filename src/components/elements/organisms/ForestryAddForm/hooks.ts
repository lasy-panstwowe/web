import { IOption } from 'components/elements/atoms'
import { useState } from 'react'
import { IPoint } from '../PointForm/interfaces'

export interface ForestryDTO {
  name: string
  districtId: string
  coords: Array<{ lng: number; lat: number }>
}
export interface ForestryDeviceDTO {
  name: string
  deviceId: string
  forestry: string
}
export const useForestryCreator = () => {
  const [name, setName] = useState('')
  const [district, setDistrict] = useState<IOption | null>(null)
  const [coords, setCoords] = useState<IPoint[]>([])

  const forestry = {
    name,
    district,
    coords,
  }

  const forestryDTO: ForestryDTO = {
    name,
    districtId: district?.value || '',
    coords: coords.map(({ longtitude, latitude }) => ({ lng: longtitude, lat: latitude })),
  }

  return { forestry, setName, setDistrict, setCoords, forestryDTO }
}

export const useForestryDeviceCreator = () => {
  const [name, setName] = useState('')
  const [deviceId, setDeviceId] = useState('')
  const [forestry, setForestry] = useState('')

  const forestryDevice = {
    name,
    deviceId,
    forestry,
  }

  const forestryDeviceDTO: ForestryDeviceDTO = {
    name,
    deviceId,
    forestry,
  }

  return { forestryDevice, setName, setDeviceId, setForestry, forestryDeviceDTO }
}
