import WarningIcon from 'assets/icons/warning.svg'
import { Card } from 'components/elements/atoms'
import { PaginationElement } from 'components/elements/molecules'
import { TableConfig, TableElement } from 'components/elements/molecules/Table/Table'
import { format } from 'date-fns'
import * as React from 'react'
import styled from 'styled-components'
import { IForestryDevice } from 'types/ForestryDevice'

export const Img = styled.img`
  height: 30px;
  min-width: 40px;
  margin-right: 25px;
`

function createData({ _id, name, type, forestry, lastTime }: IForestryDevice) {
  return {
    id: _id,
    name,
    type,
    forestry,
    last_time: lastTime ? format(new Date(lastTime), 'yyyy-MM-dd HH:mm:ss') : 'N/A',
    alarm: <Img src={WarningIcon} alt="ads" />,
  }
}

export const DevicesList: React.FunctionComponent<{
  devices: IForestryDevice[]
  paginationSize: number
  defaultValue: number
  onPaginationChange: (value: number) => void
  showDetails?: (id: string) => void
}> = ({ devices, paginationSize, defaultValue, onPaginationChange, showDetails }) => {
  const config: TableConfig[] = [
    { field: 'id', label: 'ID', identifier: true },
    { field: 'name', label: 'Nazwa' },
    { field: 'type', label: 'Typ' },
    { field: 'forestry', label: 'Leśnictwo' },
    { field: 'last_time', label: 'Ostatnia aktualizacja danych' },
    { field: 'alarm', label: 'Alarm' },
  ]
  const rows = devices.map(createData)
  const onChange = (event: React.ChangeEvent<unknown>, value: number) => onPaginationChange(value)
  return (
    <Card>
      <TableElement
        isShortID
        isCheckbox={false}
        config={config}
        rows={rows}
        showDetails={showDetails}
      />
      <PaginationElement defaultValue={defaultValue} onChange={onChange} count={paginationSize} />
    </Card>
  )
}
