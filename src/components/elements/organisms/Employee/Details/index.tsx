import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import {
  Box,
  DrawerContent,
  cssClass,
  ValueGroup,
  H2,
  ButtonCSS,
  ButtonProps,
  Icon,
  ButtonGroup,
} from '@adminjs/design-system'
import styled from 'styled-components'
import { Breadcrumbs } from '../../../molecules'

const Details: React.FunctionComponent<Record<string, never>> = () => {
  const worker = {
    name: 'name',
    surname: 'surname',
    phone: 'phone',
    email: 'email',
    employeeType: 'employeeType',
    forestInspectorate: 'forest_inspectorate',
    district: 'district',
    forestry: 'forestry',
  }

  return (
    <DrawerContent>
      <Box flexGrow={1}>
        <ValueGroup label="Imię">{worker.name}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Nazwisko">{worker.surname}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Nr telefonu">{worker.phone}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Email">{worker.email}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Stanowisko">{worker.employeeType}</ValueGroup>
      </Box>
      {worker.forestry && (
        <>
          <Box flexGrow={1}>
            <ValueGroup label="Nadleśnictwo">{worker.forestInspectorate}</ValueGroup>
          </Box>
          <Box flexGrow={1}>
            <ValueGroup label="Obręb">{worker.district}</ValueGroup>
          </Box>
          <Box flexGrow={1}>
            <ValueGroup label="Nadleśnictwo">{worker.forestry}</ValueGroup>
          </Box>
        </>
      )}
    </DrawerContent>
  )
}

const DetailsHeader: React.FunctionComponent<Record<string, never>> = () => {
  const StyledLink = styled(({ rounded, ...rest }) => <RouterLink {...rest} />)<ButtonProps>`
    ${ButtonCSS}
  `

  const buttons = [
    {
      label: 'Edytuj',
      href: {},
      icon: 'Edit',
    },
    {
      label: 'Usuń',
      onClick: {},
      variant: 'danger',
      icon: 'TrashCan',
    },
  ]

  return (
    <DrawerContent>
      <Box className={cssClass('ActionHeader')}>
        <Box flex flexDirection="col" px={['default', 0]}>
          <Breadcrumbs
            resourceName="Pracownik"
            resourceHref=""
            actionName="Wyświetl dane pracownika"
          />
        </Box>
      </Box>
      <Box display={['block', 'flex']}>
        <Box mt="lg" flexGrow={1} px={['default', 0]}>
          <H2>
            <StyledLink size="icon" to="" rounded mr="lg" type="button">
              <Icon icon="ChevronLeft" />
            </StyledLink>
            Wyświetl dane pracownika
          </H2>
        </Box>
        <Box mt="xl" flexShrink={0}>
          <ButtonGroup size="md" buttons={buttons} />
        </Box>
      </Box>
    </DrawerContent>
  )
}

export { Details, DetailsHeader }
