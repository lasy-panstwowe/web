import React from 'react'
import {
  Box,
  Button,
  DrawerContent,
  selectStyles,
  FormGroup,
  Input,
  Label,
  DrawerFooter,
} from '@adminjs/design-system'
import Select from 'react-select'
import { theme } from '../../../../../theme'

const EmployeeForm: React.FunctionComponent<Record<string, never>> = () => {
  const options = [
    { value: '1', label: 'option1' },
    { value: '2', label: 'option2' },
    { value: '3', label: 'option3' },
  ]

  const styles = selectStyles(theme)

  return (
    <Box as="form" onSubmit="" flex flexGrow={1} flexDirection="column">
      <DrawerContent>
        <FormGroup>
          <Label htmlFor="name-input" required="True">
            Imię
          </Label>
          <Input id="name-input" name="firstname" variant="default" />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="surname-input" required="True">
            Nazwisko
          </Label>
          <Input id="surname-input" name="surname" />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="phone-input" required="True">
            Nr telefonu
          </Label>
          <Input id="phone-input" name="phone" />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="phone-input" required="True">
            Email
          </Label>
          <Input id="email-input" name="email" />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="employee-type-input" required="True">
            Stanowisko
          </Label>
          <Select
            isClearable
            styles={styles}
            options={options}
            onChange={() => {}}
            id="employee-type-input"
            name="employee-type"
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="inspectorate-input">Nadleśnictwo</Label>
          <Select
            isClearable
            styles={styles}
            options={options}
            onChange={() => {}}
            id="inspectorate-input"
            name="forest_inspectorate"
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="district-input">Obręb</Label>
          <Select
            isClearable
            styles={styles}
            options={options}
            onChange={() => {}}
            id="district-input"
            name="district"
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="forestry-input">Leśnictwo</Label>
          <Select
            isClearable
            styles={styles}
            options={options}
            onChange={() => {}}
            id="forestry-input"
            name="forestry"
          />
        </FormGroup>
      </DrawerContent>
      <DrawerFooter>
        <Button variant="primary" size="lg" type="submit" data-testid="button-save">
          Zapisz
        </Button>
      </DrawerFooter>
    </Box>
  )
}

export { EmployeeForm }
