import React from 'react'
import {
  Table,
  TableBody,
  Box,
  CheckBox,
  TableHead,
  TableRow,
  TableCell,
  Pagination,
  Text,
  DrawerContent,
  cssClass,
  H2,
  Badge,
  ButtonGroup,
} from '@adminjs/design-system'
import { TableMenuDropdown } from '../../../molecules/TableMenuDropdown'
import { Breadcrumbs } from '../../../molecules'

const EmployeeTable: React.FunctionComponent<Record<string, never>> = () => {
  const records = [
    {
      id: 'id1',
      name: 'name1',
      surname: 'option1',
      phone: 'phone1',
      email: 'email1',
    },
    {
      id: 'id2',
      name: 'name2',
      surname: 'option2',
      phone: 'phone2',
      email: 'email2',
    },
  ]

  const handleClick = () => {
    alert('clicked')
  }

  return (
    <Box variant="white">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <CheckBox style={{ marginLeft: 5 }} onChange={() => {}} />
            </TableCell>
            <TableCell className="main" key="name-header" display="">
              Imię
            </TableCell>
            <TableCell className="main" key="surname-header" display="">
              Nazwisko
            </TableCell>
            <TableCell className="main" key="phone-header" display="">
              Nr telefonu
            </TableCell>
            <TableCell className="main" key="email-header" display="">
              Email
            </TableCell>
            <TableCell key="actions" style={{ width: 80 }} />
          </TableRow>
        </TableHead>
        <TableBody>
          {records.map((record) => (
            <TableRow onClick={() => {}} key={record.id}>
              <TableCell>
                <CheckBox onChange={() => {}} key={`checkbox${record.id}`} />
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`name${record.id}`}
                data-property-name=""
                display=""
              >
                {record.name}
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`surname${record.id}`}
                data-property-name=""
                display=""
              >
                {record.surname}
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`phone${record.id}`}
                data-property-name=""
                display=""
              >
                {record.phone}
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`email${record.id}`}
                data-property-name=""
                display=""
              >
                {record.email}
              </TableCell>
              <TableMenuDropdown
                resource="employee"
                recordId={record.id}
                onClickDelete={handleClick}
              />
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Text mt="xl" textAlign="center">
        <Pagination total={records.length} page={1} perPage={10} onChange={(item) => item} />
      </Text>
    </Box>
  )
}

const ListHeader: React.FunctionComponent<Record<string, never>> = () => {
  const buttons = [
    {
      label: 'Dodaj pracownika leśnictwa',
      variant: 'primary',
      href: {},
    },
    {
      label: 'Filtruj',
      onClick: {},
      variant: 'default',
      icon: 'SettingsAdjust',
    },
  ]

  const actionName = 'Lista'
  const recordsNumber = '10'

  return (
    <DrawerContent>
      <Box className={cssClass('ActionHeader')}>
        <Box flex flexDirection="col" px={['default', 0]}>
          <Breadcrumbs
            resourceName="Pracownicy leśnictwa"
            resourceHref=""
            actionName={actionName}
          />
        </Box>
      </Box>
      <Box display={['block', 'flex']}>
        <Box mt="lg" flexGrow={1} px={['default', 0]}>
          <H2>
            {actionName}
            <Badge ml="lg" variant="primary" size="lg">
              {recordsNumber}
            </Badge>
          </H2>
        </Box>
        <Box mt="xl" flexShrink={0}>
          <ButtonGroup size="md" buttons={buttons} />
        </Box>
      </Box>
    </DrawerContent>
  )
}

export { EmployeeTable, ListHeader }
