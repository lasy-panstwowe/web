import React from 'react'
import Select from 'react-select'
import {
  Box,
  H3,
  Button,
  Icon,
  Drawer,
  DrawerContent,
  DrawerFooter,
  FormGroup,
  Input,
  Label,
  filterStyles,
} from '@adminjs/design-system'
import { theme } from '../../../../../theme'

export const FilterPanel: React.FunctionComponent<Record<string, never>> = () => {
  const options = [
    { value: '1', label: 'option1' },
    { value: '2', label: 'option2' },
    { value: '3', label: 'option3' },
  ]

  return (
    <Drawer variant="filter" isHidden="" as="form" onSubmit="">
      <DrawerContent>
        <H3>
          <Button type="button" size="icon" rounded mr="lg" onClick="">
            <Icon icon="ChevronRight" color="white" />
          </Button>
          Filtrowanie
        </H3>
        <Box my="x3">
          <FormGroup variant="filter">
            <Label>Nazwa</Label>
            <Input id="name-input" name="name" />
          </FormGroup>
          <FormGroup>
            <Label>Nadleśnictwo</Label>
            <Select
              isClearable
              styles={filterStyles(theme)}
              options={options}
              onChange={() => {}}
              id="inspectorate-input"
              name="forest_inspectorate"
            />
          </FormGroup>
          <FormGroup>
            <Label>Obręb</Label>
            <Select
              isClearable
              styles={filterStyles(theme)}
              options={options}
              onChange={() => {}}
              id="district-input"
              name="district"
            />
          </FormGroup>
        </Box>
      </DrawerContent>
      <DrawerFooter>
        <Button variant="primary" size="lg">
          Zastosuj
        </Button>
        <Button variant="text" size="lg" onClick={() => {}} type="button" color="white">
          Anuluj
        </Button>
      </DrawerFooter>
    </Drawer>
  )
}
