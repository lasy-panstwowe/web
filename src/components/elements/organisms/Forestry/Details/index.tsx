import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import {
  Box,
  DrawerContent,
  cssClass,
  ValueGroup,
  H2,
  ButtonCSS,
  ButtonProps,
  Icon,
  ButtonGroup,
  H1,
} from '@adminjs/design-system'
import styled from 'styled-components'
import { Breadcrumbs } from '../../../molecules'

export const Details: React.FunctionComponent<Record<string, never>> = () => {
  const forestry = {
    id: 'id',
    name: 'name',
    inspectorate: 'inspectorate',
    district: 'district',
    cords: [
      { lat: 52.53528581125016, long: 52.53528581125016 },
      { lat: 52.53528581125016, long: 52.53528581125016 },
      { lat: 52.53528581125016, long: 52.53528581125016 },
    ],
  }

  return (
    <DrawerContent>
      <Box flexGrow={1}>
        <ValueGroup label="Nazwa">{forestry.name}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Nadleśnictwo">{forestry.inspectorate}</ValueGroup>
      </Box>
      <Box flexGrow={1}>
        <ValueGroup label="Obręb">{forestry.district}</ValueGroup>
      </Box>
      <Box>
        <H1>MAPA</H1>
      </Box>
    </DrawerContent>
  )
}

export const DetailsHeader: React.FunctionComponent<Record<string, never>> = () => {
  const StyledLink = styled(({ rounded, ...rest }) => <RouterLink {...rest} />)<ButtonProps>`
    ${ButtonCSS}
  `

  const buttons = [
    {
      label: 'Edytuj',
      href: {},
      icon: 'Edit',
    },
    {
      label: 'Usuń',
      onClick: {},
      variant: 'danger',
      icon: 'TrashCan',
    },
  ]

  return (
    <DrawerContent>
      <Box className={cssClass('ActionHeader')}>
        <Box flex flexDirection="col" px={['default', 0]}>
          <Breadcrumbs
            resourceName="Leśnictwo"
            resourceHref=""
            actionName="Wyświetl szczegóły leśnictwa"
          />
        </Box>
      </Box>
      <Box display={['block', 'flex']}>
        <Box mt="lg" flexGrow={1} px={['default', 0]}>
          <H2>
            <StyledLink size="icon" to="" rounded mr="lg" type="button">
              <Icon icon="ChevronLeft" />
            </StyledLink>
            Wyświetl szczegóły leśnictwa
          </H2>
        </Box>
        <Box mt="xl" flexShrink={0}>
          <ButtonGroup size="md" buttons={buttons} />
        </Box>
      </Box>
    </DrawerContent>
  )
}
