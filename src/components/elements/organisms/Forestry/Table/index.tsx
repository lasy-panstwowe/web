import React from 'react'
import {
  Table,
  TableBody,
  Box,
  CheckBox,
  TableHead,
  TableRow,
  TableCell,
  Pagination,
  Text,
  DrawerContent,
  cssClass,
  H2,
  Badge,
  ButtonGroup,
} from '@adminjs/design-system'
import { TableMenuDropdown } from '../../../molecules/TableMenuDropdown'
import { Breadcrumbs } from '../../../molecules'

const ForestryTable: React.FunctionComponent<Record<string, never>> = () => {
  const records = [
    {
      id: 'id1',
      name: 'name1',
      inspectorate: 'inspectorate1',
      district: 'district1',
    },
    {
      id: 'id2',
      name: 'name2',
      inspectorate: 'inspectorate2',
      district: 'district2',
    },
    {
      id: 'id2',
      name: 'name2',
      inspectorate: 'inspectorate2',
      district: 'district2',
    },
  ]

  const handleClick = () => {
    alert('clicked')
  }

  return (
    <Box variant="white">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <CheckBox style={{ marginLeft: 5 }} onChange={() => {}} />
            </TableCell>
            <TableCell className="main" key="name-header" display="">
              Nazwa
            </TableCell>
            <TableCell className="main" key="inspectorate-header" display="">
              Nadleśnictwo
            </TableCell>
            <TableCell className="main" key="district-header" display="">
              Obręb
            </TableCell>
            <TableCell key="actions" style={{ width: 80 }} />
          </TableRow>
        </TableHead>
        <TableBody>
          {records.map((record) => (
            <TableRow onClick={() => {}} key={record.id}>
              <TableCell>
                <CheckBox onChange={() => {}} key={`checkbox${record.id}`} />
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`name${record.id}`}
                data-property-name=""
                display=""
              >
                {record.name}
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`inspectorate${record.id}`}
                data-property-name=""
                display=""
              >
                {record.inspectorate}
              </TableCell>
              <TableCell
                style={{ cursor: 'pointer' }}
                key={`district${record.id}`}
                data-property-name=""
                display=""
              >
                {record.district}
              </TableCell>
              <TableCell>
                <TableMenuDropdown
                  resource="forestry"
                  recordId={record.id}
                  onClickDelete={handleClick}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Text mt="xl" textAlign="center">
        <Pagination total={records.length} page={1} perPage={10} onChange={(item) => item} />
      </Text>
    </Box>
  )
}

const ListHeader: React.FunctionComponent<Record<string, never>> = () => {
  const buttons = [
    {
      label: 'Dodaj leśnictwo',
      variant: 'primary',
      href: {},
    },
    {
      label: 'Filtruj',
      onClick: {},
      variant: 'default',
      icon: 'SettingsAdjust',
    },
  ]

  const actionName = 'Lista'
  const recordsNumber = '10'

  return (
    <DrawerContent>
      <Box className={cssClass('ActionHeader')}>
        <Box flex flexDirection="col" px={['default', 0]}>
          <Breadcrumbs resourceName="Leśnictwo" resourceHref="" actionName={actionName} />
        </Box>
      </Box>
      <Box display={['block', 'flex']}>
        <Box mt="lg" flexGrow={1} px={['default', 0]}>
          <H2>
            {actionName}
            <Badge ml="lg" variant="primary" size="lg">
              {recordsNumber}
            </Badge>
          </H2>
        </Box>
        <Box mt="xl" flexShrink={0}>
          <ButtonGroup size="md" buttons={buttons} />
        </Box>
      </Box>
    </DrawerContent>
  )
}

export { ForestryTable, ListHeader }
