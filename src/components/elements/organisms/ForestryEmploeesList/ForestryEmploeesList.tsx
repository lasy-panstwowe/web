import * as React from 'react'
import { Person } from 'types/Person'
import { Card } from 'components/elements/atoms'
import { TableConfig, TableElement } from 'components/elements/molecules/Table/Table'

function createData({ id, name, surname, phone, email }: Person) {
  return { id, name, surname, phone, email }
}

export const ForestryEmploeeList: React.FunctionComponent<{ emploees: Person[] }> = ({
  emploees,
}) => {
  const config: TableConfig[] = [
    { field: 'id', label: '', identifier: true, omit: true },
    { field: 'name', label: 'Imię' },
    { field: 'surname', label: 'Nazwisko' },
    { field: 'phone', label: 'Nr telefonu' },
    { field: 'email', label: 'Email' },
  ]
  const rows = emploees.map(createData)

  return (
    <Card>
      <TableElement config={config} rows={rows} />
      <div>Pagination</div>
    </Card>
  )
}
