import { Card } from 'components/elements/atoms'
import { PaginationElement } from 'components/elements/molecules'
import { TableConfig, TableElement } from 'components/elements/molecules/Table/Table'
import * as React from 'react'
import { IFullForestry } from 'types/Forestry'

function createData({ forestry, currentInspectorate, currentDistrict }: IFullForestry) {
  return {
    id: forestry._id,
    name: forestry.name,
    forest_inspectorate: currentInspectorate?.name,
    district: currentDistrict?.name,
  }
}

export const ForestryList: React.FunctionComponent<{
  forestries: IFullForestry[]
  paginationSize: number
  defaultValue: number
  onPaginationChange: (value: number) => void
  deleteElement?: (id: string) => void
  showDetails?: (id: string) => void
  editElement?: (id: string) => void
}> = ({
  forestries,
  paginationSize,
  defaultValue,
  onPaginationChange,
  deleteElement,
  showDetails,
  editElement,
}) => {
  const config: TableConfig[] = [
    { field: 'id', label: '', identifier: true, omit: true },
    { field: 'name', label: 'Nazwa' },
    { field: 'forest_inspectorate', label: 'Nadleśnictwo' },
    { field: 'district', label: 'Obręb' },
  ]
  const rows = forestries.map(createData)
  const onChange = (event: React.ChangeEvent<unknown>, value: number) => onPaginationChange(value)
  return (
    <Card>
      <TableElement
        config={config}
        rows={rows}
        deleteElement={deleteElement}
        showDetails={showDetails}
        editElement={editElement}
      />
      <PaginationElement defaultValue={defaultValue} onChange={onChange} count={paginationSize} />
    </Card>
  )
}
