import React, { useEffect } from 'react'
import { Button, Map } from 'components/elements/atoms'
import { PointChangedEvent, PointInput } from 'components/elements/molecules'
import styled from 'styled-components'
import { usePoints } from './hooks'
import { IPoint } from './interfaces'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 25px;
`

const MapContainer = styled.div`
  flex: 1;
  width: 100%;
`

const PointsContainer = styled.div`
  flex: 1;
`

const ButtonWrapper = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: flex-end;
`

export interface IPointForm {
  defaultPoints?: IPoint[]
  onPointsChange(points: IPoint[]): void
}

export const PointForm: React.FunctionComponent<IPointForm> = ({
  defaultPoints,
  onPointsChange,
}) => {
  const { points, addPoint, editPoint, removePoint } = usePoints()

  useEffect(() => {
    if (defaultPoints) onPointsChange(defaultPoints)
  }, [defaultPoints, onPointsChange])

  useEffect(() => {
    onPointsChange(points)
  }, [points, onPointsChange])

  const handleLatitudeChange = (e: PointChangedEvent) => {
    editPoint(e.id, 'latitude', e.value)
  }

  const handleLongtitudeChange = (e: PointChangedEvent) => {
    editPoint(e.id, 'longtitude', e.value)
  }

  const handlePointDelete = (e: PointChangedEvent) => {
    removePoint(e.id)
  }

  return (
    <Wrapper>
      <MapContainer>
        <Map points={points.map(({ latitude, longtitude }) => [latitude, longtitude])} />
      </MapContainer>
      <PointsContainer>
        <ButtonWrapper>
          <Button onClick={addPoint}>Dodaj nowy punkt</Button>
        </ButtonWrapper>
        {points.map(({ id, latitude, longtitude }) => (
          <PointInput
            key={id}
            id={id}
            latitude={latitude}
            longitude={longtitude}
            onLatitudeChange={handleLatitudeChange}
            onLongtitudeChange={handleLongtitudeChange}
            onClickDelete={handlePointDelete}
          />
        ))}
      </PointsContainer>
    </Wrapper>
  )
}
