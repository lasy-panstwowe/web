import React from 'react'
import { IPoint } from './interfaces'

export const usePoints = () => {
  const [points, setPoints] = React.useState<IPoint[]>([])
  const [counter, setCounter] = React.useState(1)

  const addPoint = () => {
    setPoints([...points, { id: counter, latitude: 0, longtitude: 0 }])
    setCounter(counter + 1)
  }

  const editPoint = (id: number, field: 'latitude' | 'longtitude', value: number) => {
    const original = points.find((p) => p.id === id)
    if (!original) {
      throw Error('Cannot edit point that does not exist')
    }
    const point: IPoint = { ...original, [field]: value }
    setPoints([...points.filter((p) => p.id !== id), point].sort((p1, p2) => p1.id - p2.id))
  }

  const removePoint = (id: number) => {
    setPoints([...points.filter((p) => p.id !== id)])
  }

  return { points, addPoint, editPoint, removePoint }
}
