export interface IPoint {
  id: number
  latitude: number
  longtitude: number
}
