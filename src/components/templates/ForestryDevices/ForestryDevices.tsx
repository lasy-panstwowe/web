import {
  Breadcrumbs,
  Button,
  Count,
  DevicesList,
  FilterDrawer,
  InputWithLabel,
  Title,
} from 'components/elements'
import { useForestryDeviceCreator } from 'components/elements/organisms/ForestryAddForm/hooks'
import React, { useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'
import { Forestry } from 'types/Forestry'
import { IForestryDevice } from 'types/ForestryDevice'

const PAGINATION_SIZE = 10
const DEFAULT_PAGE = 1
const Wrapper = styled.section`
  padding: 27px 54px 33px 32px;
`

const StyledHeader = styled.header`
  margin: 19px 0px;
  display: flex;
  justify-content: space-between;
`

const ButtonWrapper = styled.div`
  display: flex;
  gap: 25px;
`

interface ForestryProps {
  devices: IForestryDevice[]
  forestries: Forestry[]
  showForestryDetails: (id: string) => void
}

export const ForestryDevices: React.FunctionComponent<ForestryProps> = ({
  devices,
  forestries,
  showForestryDetails,
}) => {
  const [currentDevices, setCurrentDevices] = useState<IForestryDevice[]>([])
  const [allDevices, setAllDevices] = useState<IForestryDevice[]>([])

  const paginationCount = devices.length % PAGINATION_SIZE

  const onPaginationChange = useCallback(
    (value: number) => {
      const currentPage = value - 1

      setCurrentDevices(
        allDevices.slice(
          currentPage * PAGINATION_SIZE,
          currentPage * PAGINATION_SIZE + PAGINATION_SIZE
        )
      )
    },
    [allDevices]
  )

  useEffect(() => {
    const currentDevicesList = devices.map((device) => {
      const currentForestry = forestries.find((forestry) => forestry._id === device.forestry)
      return { ...device, forestry: currentForestry?.name || 'N/A' }
    })
    setAllDevices(currentDevicesList)
  }, [forestries, devices])

  useEffect(() => {
    onPaginationChange(DEFAULT_PAGE)
  }, [onPaginationChange])

  const [openFilter, setOpenFilter] = useState(false)
  const { forestryDevice, forestryDeviceDTO, setName, setDeviceId, setForestry } =
    useForestryDeviceCreator()

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const handleDeviceIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDeviceId(e.target.value)
  }
  const handleForestryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForestry(e.target.value)
  }

  const onResetClick = () => {
    setName('')
    setDeviceId('')
    setForestry('')
    const currentDevicesList = devices.map((device) => {
      const currentForestry = forestries.find((forestry) => forestry._id === device.forestry)
      return { ...device, forestry: currentForestry?.name || 'N/A' }
    })
    setAllDevices(currentDevicesList)
    setOpenFilter(false)
  }
  const onHideFilterClick = () => {
    setOpenFilter(false)
  }

  const onSubmitClick = () => {
    let allDevicesList = allDevices
    if (forestryDeviceDTO.name) {
      allDevicesList = allDevices.filter((element) => element.name === forestryDeviceDTO.name)
    }

    if (forestryDeviceDTO.deviceId) {
      allDevicesList = allDevices.filter(
        (element) => element._id.slice(0, 6).toUpperCase() === forestryDeviceDTO.deviceId
      )
    }

    if (forestryDeviceDTO.forestry) {
      allDevicesList = allDevices.filter(
        (element) => element.forestry === forestryDeviceDTO.forestry
      )
    }
    setAllDevices(allDevicesList)
    setOpenFilter(false)
  }

  return (
    <Wrapper>
      <Breadcrumbs resourceName="Czujniki" actionName="Lista" resourceHref="/forestry-devices" />
      <StyledHeader>
        <div>
          <Title>Lista</Title> <Count>{devices.length}</Count>
        </div>
        <ButtonWrapper>
          <Button onClick={() => setOpenFilter(true)}>Filtruj</Button>
        </ButtonWrapper>
      </StyledHeader>
      <DevicesList
        showDetails={showForestryDetails}
        defaultValue={DEFAULT_PAGE}
        paginationSize={paginationCount}
        devices={currentDevices}
        onPaginationChange={onPaginationChange}
      />
      <FilterDrawer
        hideFilter={onHideFilterClick}
        onResetClick={onResetClick}
        onSubmitClick={onSubmitClick}
        open={openFilter}
      >
        <InputWithLabel
          id="name-input"
          name="ID"
          value={forestryDevice.deviceId}
          onChange={handleDeviceIdChange}
        />
        <InputWithLabel
          id="name-input"
          name="Nazwa"
          value={forestryDevice.name}
          onChange={handleNameChange}
        />
        <InputWithLabel
          id="name-input"
          name="Leśnictwo"
          value={forestryDevice.forestry}
          onChange={handleForestryChange}
        />
      </FilterDrawer>
    </Wrapper>
  )
}
