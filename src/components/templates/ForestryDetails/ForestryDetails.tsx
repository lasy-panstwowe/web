import { Box } from '@mui/material'
import { Breadcrumbs, Button, Card, Map, Text, Title } from 'components/elements'
import React from 'react'
import styled from 'styled-components'
import { IDistrict } from 'types/District'
import { Forestry } from 'types/Forestry'
import { IForestryInspectorate } from 'types/ForestryInspectorate'

const StyledHeader = styled.header`
  margin: 10px 0px;
  display: flex;
  justify-content: space-between;
`

export const ForestryDetails: React.FunctionComponent<{
  forestry: Forestry
  district: IDistrict
  inspectorate: IForestryInspectorate
  deleteForestry: () => void
  editForestry: () => void
}> = ({ forestry, district, inspectorate, deleteForestry, editForestry }) => {
  const resourceHref = `/forestry/`
  return (
    <section>
      <Breadcrumbs
        resourceName="leśnictwo"
        actionName="Szczegóły leśnictwa"
        resourceHref={resourceHref}
      />
      <StyledHeader>
        <Title>Szczegóły leśnictwa</Title>
        <Box sx={{ display: 'flex', flexDirection: 'row' }}>
          <Button onClick={editForestry}>Edytuj</Button>
          <Box
            sx={{
              width: 8,
            }}
          />
          <Button onClick={deleteForestry}>Usuń</Button>
        </Box>
      </StyledHeader>
      <Card>
        <Text>Nazwa:</Text>
        <Text variant="h3">{forestry.name}</Text>
        <Text>Nadleśnictwo:</Text>
        <Text variant="h3">{inspectorate.name}</Text>
        <Text>Obręb:</Text>
        <Text variant="h3">{district.name}</Text>
        <Text>Obszar:</Text>
        <Map points={forestry.coords.map(({ lat, lng }) => [lat, lng])} />
      </Card>
    </section>
  )
}
