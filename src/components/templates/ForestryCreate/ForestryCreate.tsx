import React from 'react'
import styled from 'styled-components'
import { Breadcrumbs, Card, ForestryAddForm, IForestryAddForm, Title } from 'components/elements'

const StyledHeader = styled.header`
  margin: 10px 0px;
  display: flex;
  justify-content: space-between;
`

export const ForestryCreate: React.FunctionComponent<IForestryAddForm> = ({
  districts,
  inspectorates,
  defaultDistrict,
  defaultForestry,
  defaultInspectorate,
  onSubmit,
}) => {
  return (
    <section>
      <Breadcrumbs
        resourceName="Pracownicy leśnictwa"
        actionName={defaultForestry ? 'Edytuj leśnictwo' : 'Dodaj leśnictwo'}
        resourceHref="/forestry"
      />
      <StyledHeader>
        <Title>{defaultForestry ? 'Edytuj leśnictwo' : 'Dodaj leśnictwo'} </Title>
      </StyledHeader>
      <Card>
        <ForestryAddForm
          defaultDistrict={defaultDistrict}
          defaultForestry={defaultForestry}
          defaultInspectorate={defaultInspectorate}
          districts={districts}
          inspectorates={inspectorates}
          onSubmit={onSubmit}
        />
      </Card>
    </section>
  )
}
