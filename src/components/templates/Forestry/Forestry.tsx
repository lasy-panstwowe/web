import {
  Breadcrumbs,
  Button,
  Count,
  FilterDrawer,
  InputWithLabel,
  IOption,
  SelectWithLabel,
  Title,
} from 'components/elements'
import { useForestryCreator } from 'components/elements/organisms/ForestryAddForm/hooks'
import { ForestryList } from 'components/elements/organisms/ForestryList/ForestryList'
import { FORESTRY_PATHS } from 'components/routes/paths/forestry'
import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { IDistrict } from 'types/District'
import { Forestry as IForestry, IFullForestry } from 'types/Forestry'
import { IForestryInspectorate } from 'types/ForestryInspectorate'

const PAGINATION_SIZE = 10
const DEFAULT_PAGE = 1
const Wrapper = styled.section`
  padding: 27px 54px 33px 32px;
`

const StyledHeader = styled.header`
  margin: 19px 0px;
  display: flex;
  justify-content: space-between;
`

const ButtonWrapper = styled.div`
  display: flex;
  gap: 25px;
`

interface ForestryProps {
  forestries: IForestry[]
  districts: IDistrict[]
  inspectorates: IForestryInspectorate[]
  deleteForestry: (id: string) => void
  showForestryDetails: (id: string) => void
  editForestryElement: (id: string) => void
}

export const Forestry: React.FunctionComponent<ForestryProps> = ({
  forestries,
  districts,
  inspectorates,
  deleteForestry,
  showForestryDetails,
  editForestryElement,
}) => {
  const [allForestries, setAllForestries] = useState<IFullForestry[]>([])
  const [currentForestries, setCurrentForestries] = useState<IFullForestry[]>([])
  const [openFilter, setOpenFilter] = useState(false)
  const { forestry, forestryDTO, setName, setDistrict } = useForestryCreator()
  const [currentInspectorate, setInspectorate] = useState<IOption | null>(null)
  const [inspectorateOptions, setInspectorateOptions] = useState<IOption[]>([])
  const [districtOptions, setDistrictOptions] = useState<IOption[]>([])

  const paginationCount = forestries.length % PAGINATION_SIZE

  const onPaginationChange = useCallback(
    (value: number) => {
      const currentPage = value - 1

      setCurrentForestries(
        allForestries.slice(
          currentPage * PAGINATION_SIZE,
          currentPage * PAGINATION_SIZE + PAGINATION_SIZE
        )
      )
    },
    [allForestries]
  )

  useEffect(() => {
    const currentForestriesList = forestries.map((forestryElement) => {
      const currentDistrict = districts.find(
        (district) => district._id === forestryElement.district
      )
      const currentForestryInspectorate = inspectorates.find(
        (inspectorate) => inspectorate._id === currentDistrict?.forestryInspectorate
      )
      return {
        forestry: forestryElement,
        currentDistrict,
        currentInspectorate: currentForestryInspectorate,
      }
    })
    setAllForestries(currentForestriesList)
  }, [districts, forestries, inspectorates])

  useEffect(() => {
    onPaginationChange(DEFAULT_PAGE)
  }, [onPaginationChange])

  useEffect(() => {
    setInspectorateOptions(
      inspectorates.map((inspectorate) => ({
        value: inspectorate._id,
        label: inspectorate.name,
      }))
    )
  }, [inspectorates])

  useEffect(() => {
    setDistrictOptions(
      districts
        .filter((district) => district.forestryInspectorate === currentInspectorate?.value)
        .map((district) => ({
          value: district._id,
          label: district.name,
        }))
    )
  }, [districts, currentInspectorate])

  useEffect(() => {
    if (!districtOptions.find((option) => option.value === forestry.district?.value)) {
      setDistrict(null)
    }
  }, [districtOptions, forestry, setDistrict])

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  const onInspectorateChange = (newValue: IOption) => {
    setInspectorate(newValue || null)
  }

  const onDistrictChange = (newValue: IOption) => {
    setDistrict(newValue || null)
  }

  const onResetClick = () => {
    setName('')
    setDistrict(null)
    setInspectorate(null)
    const currentForestriesList = forestries.map((forestryElement) => {
      const currentDistrict = districts.find(
        (district) => district._id === forestryElement.district
      )
      const currentForestryInspectorate = inspectorates.find(
        (inspectorate) => inspectorate._id === currentDistrict?.forestryInspectorate
      )
      return {
        forestry: forestryElement,
        currentDistrict,
        currentInspectorate: currentForestryInspectorate,
      }
    })
    setAllForestries(currentForestriesList)
    setOpenFilter(false)
  }
  const onHideFilterClick = () => {
    setOpenFilter(false)
  }
  const onSubmitClick = () => {
    let allForestriesList = allForestries
    if (forestryDTO.name) {
      allForestriesList = allForestries.filter(
        (element) => element.forestry.name === forestryDTO.name
      )
    }

    if (forestryDTO.districtId) {
      allForestriesList = allForestriesList.filter(
        (element) => element.currentDistrict?._id === forestryDTO.districtId
      )
    }

    setAllForestries(allForestriesList)
    setOpenFilter(false)
  }
  return (
    <Wrapper>
      <Breadcrumbs resourceName="Leśnictwo" actionName="Lista" resourceHref="/forestry" />
      <StyledHeader>
        <div>
          <Title>Lista</Title> <Count>{forestries.length}</Count>
        </div>
        <ButtonWrapper>
          <Link to={FORESTRY_PATHS.FORESTRY_CREATE}>
            <Button onClick={() => {}}>Dodaj Leśnictwo</Button>
          </Link>
          <Button onClick={() => setOpenFilter(true)}>Filtruj</Button>
        </ButtonWrapper>
      </StyledHeader>
      <ForestryList
        editElement={editForestryElement}
        showDetails={showForestryDetails}
        deleteElement={deleteForestry}
        defaultValue={DEFAULT_PAGE}
        paginationSize={paginationCount}
        forestries={currentForestries}
        onPaginationChange={onPaginationChange}
      />
      <FilterDrawer
        hideFilter={onHideFilterClick}
        onResetClick={onResetClick}
        onSubmitClick={onSubmitClick}
        open={openFilter}
      >
        <InputWithLabel
          id="name-input"
          name="Nazwa"
          value={forestry.name}
          onChange={handleNameChange}
        />
        <SelectWithLabel
          id="inspectorate-input"
          name="Nadleśnictwo"
          options={inspectorateOptions}
          onChange={onInspectorateChange}
        />
        <SelectWithLabel
          id="district-input"
          name="Obręb"
          value={forestry.district}
          options={districtOptions}
          onChange={onDistrictChange}
        />
      </FilterDrawer>
    </Wrapper>
  )
}
