import React from 'react'
import styled from 'styled-components'
import { Count, ForestryEmploeeList, Title, Breadcrumbs } from 'components/elements'
import { Person } from 'types/Person'

const Wrapper = styled.section`
  padding: 27px 54px 33px 32px;
`

const StyledHeader = styled.header`
  margin: 19px 0px;
  display: flex;
  justify-content: space-between;
`

export const ForestryEmploees: React.FunctionComponent<{ emploees: Person[] }> = ({ emploees }) => {
  return (
    <Wrapper>
      <Breadcrumbs
        resourceName="Pracownicy leśnictwa"
        actionName="Lista"
        resourceHref="/forestry-workers"
      />
      <StyledHeader>
        <div>
          <Title>Lista</Title> <Count>{emploees.length}</Count>
        </div>
        <div>Controls placeholder</div>
      </StyledHeader>
      <ForestryEmploeeList emploees={emploees} />
    </Wrapper>
  )
}
