import React from 'react'
import { Routes as RouterRoutes, Route, Navigate } from 'react-router-dom'

import { ROUTES } from './helpers/routes'
import { COMMON_PATHS } from './paths/common'
import { ChildrenRoutes, RouteItem } from './types.td'

const Routes = () => {
  return (
    <RouterRoutes>
      {ROUTES.map(({ path, protectedRoute: ProtectedRoute, childrenRoutes }: RouteItem) => (
        <Route key={path} path={path} element={<ProtectedRoute />}>
          {childrenRoutes.map(
            ({ path: childPath, element: ChildElement, layout: Layout }: ChildrenRoutes) => (
              <Route key={childPath} path={path} element={<Layout />}>
                <Route key={childPath} path={childPath} element={<ChildElement />} />
              </Route>
            )
          )}
        </Route>
      ))}
      <Route path="/*" element={<Navigate to={COMMON_PATHS.NOT_FOUND} replace />} />
    </RouterRoutes>
  )
}

export default Routes
