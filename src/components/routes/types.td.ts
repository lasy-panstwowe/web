export type ChildrenRoutes = {
  path: string
  element: React.FunctionComponent
  layout: React.FunctionComponent
}

export type RouteItem = {
  path: string
  protectedRoute: React.FunctionComponent
  childrenRoutes: ChildrenRoutes[]
}
