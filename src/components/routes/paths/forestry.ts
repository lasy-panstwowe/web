const ROOT = '/forestry'

export const FORESTRY_PATHS = {
  FORESTRY_LIST: ROOT,
  FORESTRY_CREATE: `${ROOT}/create`,
  FORESTRY_DETAILS: `${ROOT}/:id`,
  FORESTRY_EDIT: `${ROOT}/edit/:id`,
}
