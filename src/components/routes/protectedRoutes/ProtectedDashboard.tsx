import React from 'react'
import { Outlet } from 'react-router-dom'

const ProtectedDashboard: React.FunctionComponent = () => {
  // return isAuth ? <Outlet /> : <Navigate to="/login" />;
  return <Outlet />
}

export default ProtectedDashboard
