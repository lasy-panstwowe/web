import { DASHBOARD_ROUTES } from './dashboard_routes'
import ProtectedDashboard from '../protectedRoutes/ProtectedDashboard'
import { RouteItem } from '../types.td'
import { DASHBOARD_PATHS } from '../paths/dashboard'

export const ROUTES: RouteItem[] = [
  {
    path: DASHBOARD_PATHS.ROOT,
    protectedRoute: ProtectedDashboard,
    childrenRoutes: DASHBOARD_ROUTES,
  },
  // {
  //   routeRenderer: Route, // to create Protected Routes
  //   path: '',
  //   component: null,
  // },
  // {
  //   routeRenderer: Route, // to create Protected Routes
  //   path: 'forestry',
  //   component: Forestry,
  // },
  // {
  //   routeRenderer: Route,
  //   path: COMMON_PATHS.NOT_FOUND,
  //   component: NotFoundView,
  // },
  // {
  //   routeRenderer: Route,
  //   path: '/forestry-workers',
  //   component: ForestryEmploeesPage,
  // },
]
