import { MainLayout } from 'components/layouts'

import { Forestry } from 'views/Forestry/Forestry'
import { ForestryCreateView } from 'views/ForestryCreate/ForestryCreate'
import { ForestryDetailsView } from 'views/ForestryDetails/ForestryDetails'
import { ForestryEmploeesPage } from 'views/ForestryEmploees/ForestryEmplyees'
import { ForestryDevices } from 'views/ForestryDevices/ForestryDevices'
import NotFound from 'views/NotFound/NotFound'
import { EMPLOYEE_PATHS } from '../paths/employee'
import { FORESTRY_PATHS } from '../paths/forestry'
import { FORESTRY_DEVICES_PATHS } from '../paths/forestry-devices'
import { ChildrenRoutes } from '../types.td'

export const DASHBOARD_ROUTES: ChildrenRoutes[] = [
  {
    path: FORESTRY_PATHS.FORESTRY_LIST,
    layout: MainLayout,
    element: Forestry,
  },
  {
    path: FORESTRY_PATHS.FORESTRY_CREATE,
    layout: MainLayout,
    element: ForestryCreateView,
  },
  {
    path: FORESTRY_PATHS.FORESTRY_EDIT,
    layout: MainLayout,
    element: ForestryCreateView,
  },
  {
    path: FORESTRY_PATHS.FORESTRY_DETAILS,
    layout: MainLayout,
    element: ForestryDetailsView,
  },
  {
    path: FORESTRY_DEVICES_PATHS.FORESTRY_DEVICES_LIST,
    layout: MainLayout,
    element: ForestryDevices,
  },
  {
    path: EMPLOYEE_PATHS.WORKERS_LIST,
    layout: MainLayout,
    element: ForestryEmploeesPage,
  },
  {
    path: '/*',
    layout: MainLayout,
    element: NotFound.Component,
  },
]
