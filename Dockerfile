FROM node:16-alpine as ci
RUN apk add --no-cache tzdata
ENV TZ "Europe/Warsaw"
WORKDIR /app

COPY . .
RUN yarn install
RUN yarn global add react-scripts@3.0.1

FROM ci as artifact
RUN yarn build 

FROM nginx:latest
ENV TZ "Europe/Warsaw"
COPY --from=artifact /app/build /usr/share/nginx/html
EXPOSE 80
